#pragma once

#include "cocos2d.h"

class Events {
public:
  static std::string TOUCHED_CALENDER_BUTTON;
  static std::string SELECTED_DAY_CELL;

  // メイン画面
  static std::string TOUCHED_NEXT_CALENDER_BUTTON;
  static std::string SELECTED_MOVE_CALENDER_PAGE;
  static std::string TOUCHED_TODAY_CHECK_BUTTON;
  static std::string SELECTED_CALENDER_CELL;
  static std::string SELECTED_STAMP;

  // 設定画面
  static std::string TOUCHED_CLOSE_SETTING_BUTTON;

  // メニュー画面
  static std::string TOUCHED_CLOSE_MENU_BUTTON;
  static std::string TOUCHED_CREATE_CALENDER_BUTTON;
  static std::string SELECTED_OPEN_CALENDER;
  static std::string SELECTED_DELETE_CALENDER;

  // デリートモーダル
  static std::string TOUCHED_DELETE_CANCEL_BUTTON;
  static std::string TOUCHED_DELETE_FIX_BUTTON;

  // グラフ
  static std::string TOUCHED_GRAPH_BUTTON;
  static std::string TOUCHED_CLOSE_GRAPH_BUTTON;

  // データ更新
  static std::string UPDATE_TITLE;
  static std::string UPDATE_POINT;
  static std::string UPDATE_THEMA;
  static std::string MISSION_COMPLETED;
  static std::string CLOSED_MISSION_COMPLETED_EFFECT;
};