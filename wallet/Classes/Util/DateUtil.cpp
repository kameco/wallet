#include "DateUtil.h"
#include <time.h>

std::vector<std::string> DateUtil::WEEK = {"Sun", "Mon", "Tue", "Wed",
                                           "Thu", "Fri", "Sat"};

struct tm DateUtil::getCurrentTime() {
  ///////
  // http://www.c-tipsref.com/tips/time/time.html
  time_t timer = time(NULL);
  struct tm nowTime = *localtime(&timer);

  return nowTime;
}

struct tm DateUtil::getFirstDayTime(struct tm time) {
  time.tm_mday = 1;
  mktime(&time);
  return time;
}

int DateUtil::getFirstDayWeekday(struct tm time) { //
  return getFirstDayTime(time).tm_wday;
}

struct tm DateUtil::getLastDayTime(struct tm time) {
  struct tm lastDay = time;
  lastDay.tm_year = time.tm_year;
  lastDay.tm_mon = time.tm_mon + 1;
  lastDay.tm_mday = 1 - 1;
  mktime(&lastDay);

  return lastDay;
}

struct tm DateUtil::getNextDay(struct tm time) {
  auto date = time;
  ++date.tm_mday;
  mktime(&date);

  return date;
}

int DateUtil::toDateId(struct tm time) {
  auto id = (time.tm_year + 1900) * 100 + time.tm_mon + 1;
  return id;
}

struct tm DateUtil::moveMonth(struct tm time, int moveCount) {
  time.tm_mon += moveCount;
  mktime(&time);
  return time;
}

int DateUtil::getMonthDiff(struct tm begenTime, struct tm endTime) {
  auto yearDiff = endTime.tm_year - begenTime.tm_year;
  auto monthDiff = endTime.tm_mon - begenTime.tm_mon + (yearDiff * 12);
  return monthDiff;
}

int DateUtil::getWeekCountOfMonth(struct tm time) {
  auto firstDayWeek = getFirstDayTime(time).tm_wday;
  auto day = time.tm_mday - 1 + firstDayWeek;

  int weeks = floor(day / WEEK.size());

  return weeks;
};
