//
//  ActionUtil.h
//  HanaCal
//
//  Created by HidakaKaname on 2015/07/20.
//
//
#pragma once

#include "cocos2d.h"

class SlideDownTo : public cocos2d::ActionInterval {
 public:
  static FiniteTimeAction* create(const cocos2d::Vec2 position);
};

class SlideUpTo : public cocos2d::ActionInterval {
 public:
  static FiniteTimeAction* create(const cocos2d::Vec2 position);
};

class ScaleInTo : public cocos2d::ActionInterval {
 public:
  static FiniteTimeAction* create(float scale);
};

class RepeatRotate : public cocos2d::ActionInterval {
 public:
  static FiniteTimeAction* create(float direction = 30);
};
