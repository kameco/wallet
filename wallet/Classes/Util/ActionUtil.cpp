//
//  ActionUtil.cpp
//  HanaCal
//
//  Created by HidakaKaname on 2015/07/20.
//
//

#include "ActionUtil.h"

USING_NS_CC;

FiniteTimeAction* SlideDownTo::create(Vec2 position) {
  FiniteTimeAction* act;

  auto move1 = MoveTo::create(0.15, position + Vec2(0, -20));
  auto ease1 = EaseQuadraticActionIn::create(move1);
  auto move2 = MoveTo::create(0.1, position);
  auto ease2 = EaseQuadraticActionOut::create(move2);
  auto seq1 = Sequence::create(ease1, ease2, NULL);
  act = seq1;

  return act;
}

FiniteTimeAction* SlideUpTo::create(Vec2 position) {
  FiniteTimeAction* act;

  auto move1 = MoveTo::create(0.15, position + Vec2(0, 20));
  auto ease1 = EaseQuadraticActionIn::create(move1);
  auto move2 = MoveTo::create(0.1, position);
  auto ease2 = EaseQuadraticActionOut::create(move2);
  auto seq1 = Sequence::create(ease1, ease2, NULL);
  act = seq1;

  return act;
}

FiniteTimeAction* ScaleInTo::create(float scale) {
  FiniteTimeAction* act;

  auto scale1 = ScaleTo::create(0.1, scale * 1.1);
  auto ease1 = EaseQuadraticActionIn::create(scale1);

  auto scale2 = ScaleTo::create(0.03, scale);
  auto ease2 = EaseQuadraticActionOut::create(scale2);
  auto seq1 = Sequence::create(ease1, ease2, NULL);

  act = seq1;

  return act;
}

FiniteTimeAction* RepeatRotate::create(float direction) {
  FiniteTimeAction* act;

  auto rotate = RotateBy::create(0.5, direction);
  auto repeatForever = RepeatForever::create(rotate);

  act = repeatForever;

  return act;
}