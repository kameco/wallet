class DateUtil {
public:
  static struct tm getCurrentTime();
  static struct tm getFirstDayTime(struct tm time);
  static int getFirstDayWeekday(struct tm time);
  static struct tm getLastDayTime(struct tm time);
  static struct tm getNextDay(struct tm time);
  static int toDateId(struct tm time);
  static struct tm moveMonth(struct tm time, int moveCount);
  static int getMonthDiff(struct tm begenTime, struct tm endTime);
  static int getWeekCountOfMonth(struct tm time);

  static std::vector<std::string> WEEK;
};
