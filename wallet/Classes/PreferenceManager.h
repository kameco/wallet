//
//  PreferenceManager.h
//  HanaCal
//
//  Created by HidakaKaname on 2015/07/18.
//
//
#include "ChecalModel.h"

class PreferenceManager{
public:
    static ChecalModel::AllData load();
    static void save(const ChecalModel::AllData allData);
};