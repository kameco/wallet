
#include "ViewThemaModel.h"
#include "Events.h"

USING_NS_CC;

ViewThemaModel::Type ViewThemaModel::_type = ViewThemaModel::Type::Red;

ViewThemaModel::Thema ViewThemaModel::getThema(const Type type) {
  Thema thema;
  // thema.fontColor = Color3B(142, 142, 142);
    thema.fontColor = Color3B(10, 10, 10);
  thema.backgroundColor = Color3B(250, 248, 246);

  switch (type) {
    case Type::Red:
      // thema.mainColor = Color3B(228, 123, 106);
      thema.mainColor = Color3B(227, 110, 92);
      thema.stampColor = Color3B(221, 196, 143);
      break;

    case Type::Green:
      thema.mainColor = Color3B(123, 188, 201);
      thema.stampColor = Color3B(221, 196, 143);
      break;

    case Type::Blue:
      thema.mainColor = Color3B(95, 116, 153);
      thema.stampColor = Color3B(227, 175, 175);
      break;

    case Type::Glay:
      thema.mainColor = Color3B(167, 158, 151);
      thema.stampColor = Color3B(227, 175, 175);
      break;

    default:
      break;
  }
  return thema;
}

ViewThemaModel::Thema ViewThemaModel::getCurrentTheme() { return getThema(_type); }

Color3B ViewThemaModel::getMainColor() { return getCurrentTheme().mainColor; }

Color3B ViewThemaModel::getMainColor(Type type) { return getThema(type).mainColor; }

Color3B ViewThemaModel::getStampColor() { return getCurrentTheme().stampColor; }

Color3B ViewThemaModel::getStampColor(Type type) { return getThema(type).mainColor; }

Color3B ViewThemaModel::getFontColor() { return getCurrentTheme().fontColor; }

Color3B ViewThemaModel::getBackgroundColor() { return getCurrentTheme().backgroundColor; }

void ViewThemaModel::setThema(Type type) {
  _type = type;
  auto evt = EventCustom(Events::UPDATE_THEMA);
  Director::getInstance()->getEventDispatcher()->dispatchEvent(&evt);
}
