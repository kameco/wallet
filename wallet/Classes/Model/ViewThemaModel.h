#pragma once

#include "cocos2d.h"

class ViewThemaModel {
 public:
  enum class Type {
    Red = 0,
    Green = 1,
    Blue = 2,
    Glay = 3,
  };

  struct Thema {
    cocos2d::Color3B mainColor;
    cocos2d::Color3B stampColor;
    cocos2d::Color3B fontColor;
    cocos2d::Color3B backgroundColor;
  };

  static cocos2d::Color3B getMainColor();
  static cocos2d::Color3B getMainColor(const Type type);
  static cocos2d::Color3B getStampColor();
  static cocos2d::Color3B getStampColor(const Type type);
  static cocos2d::Color3B getFontColor();
  static cocos2d::Color3B getBackgroundColor();

  static void setThema(Type type);

 private:
  static Thema getCurrentTheme();
  static Thema getThema(const Type type);
  static Type _type;
};
