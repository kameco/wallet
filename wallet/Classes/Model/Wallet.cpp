#include "Wallet.h"

Wallet* Wallet::walletSingleton = NULL;
Wallet* Wallet::sharedWallet() {
    if (!walletSingleton) {
        walletSingleton = new Wallet();
    }
    
    return walletSingleton;
}

Wallet::Wallet() {
    
    for (int k = 1; k < 12; ++k) {
        MonthlyItems monthlyItems;
        for (int i = 1; i < 31; ++i) {
            DailyItems dailyItems;
            for (int j = 1; j < 5; ++j) {
                Item item;
                item.memo = "aaaaaa";
                item.money = 2016 * 100 + i * 100 + j;
                item.category = 1;
                dailyItems.push_back(item);
            }
            monthlyItems[i] = dailyItems;
        }
        _allData["monthlyData2016" + std::to_string(k)] = monthlyItems;
    }
    
}
