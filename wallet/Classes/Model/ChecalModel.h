#pragma once

#include "cocos2d.h"
#include "Stamp.h"
#include "ViewThemaModel.h"

class ChecalModel {
 public:
    struct PurchaseRecord{
        int money;
        int category;
        
        std::string memo;
    };
    using MonthlyData = std::map<int, PurchaseRecord>;
    
  static ChecalModel* sharedChecalModel();
  ChecalModel();

  using MonthData = std::map<int, Stamp::Type>;
  struct CalendarData {
    int id;
    std::string title;
    int goalPoint;
    int pointType;  // 0:全て1　1:0.5区切り
    ViewThemaModel::Type thema;
    std::map<int, MonthData> monthDataList;
  };
  struct AllData {
    std::vector<std::shared_ptr<CalendarData>> calenderDataList;
    int dispId;
  };

  void setDispId(int id);
  void setMonthData(int time, std::map<int, Stamp::Type> data);
  void setDayData(int time, int day, Stamp::Type stamp);
  void setDayData(struct tm time, Stamp::Type stamp);

  void setTitle(std::string title);
  void setGoalPoint(int goalPoint);
  void setPointType(int pointType);
  void setThema(ViewThemaModel::Type thema);
  void deleteCalendarData(int id);

  void createNewCalender();

  std::vector<std::shared_ptr<CalendarData>> getChecalModel();
  std::shared_ptr<CalendarData> getCalendarData(int id);
  std::shared_ptr<CalendarData> getCalendarData();
  Stamp::Type getTodayStamp();
  MonthData getMonthData(int time);
  MonthData getMonthData(struct tm time);
  inline int getDispId() { return _allData.dispId; };

  int getTotalPoint(struct tm time);
  int getGoalPoint();
  std::string getTitle();
  ViewThemaModel::Type getThema();

  void save();

 private:
  static ChecalModel* appDataSingleton;
  AllData load();

  AllData _allData;
};