#pragma once

#include "cocos2d.h"

class Stamp{
public:
    enum class Type{
        NONE = 0,
        TRIANGLE = 1,
        HEXAGON = 2,
        STAR = 3
    };
};