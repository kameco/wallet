#include "ChecalModel.h"
#include "ViewThemaModel.h"
#include "PreferenceManager.h"
#include "DateUtil.h"
#include "Events.h"

USING_NS_CC;

ChecalModel* ChecalModel::appDataSingleton = NULL;
ChecalModel* ChecalModel::sharedChecalModel() {
  if (!appDataSingleton) {
    appDataSingleton = new ChecalModel();
  }

  return appDataSingleton;
}

ChecalModel::ChecalModel() {
      std::string jsonStr =  "{\"appData\":["
      "{"
      "\"id\":0,"
      "\"title\":\"マッチョッチョ。\","
      "\"goalPoint\":20,"
      "\"thema\":0,"
      "\"pointType\":0,"
      "\"times\":[201506,201507],"
      "\"monthData\":{"
      "\"time201506\":{\"day1\":0,\"day2\":1,\"day3\":2,\"day4\":3,\"day5\":0,\"day6\":1,\"day7\":2,\"day8\":3},"
      "\"time201507\":{\"day1\":1,\"day2\":1,\"day3\":1}"
      "}"
      "},"
      "{"
      "\"id\":1,"
      "\"title\":\"目標２\","
      "\"goalPoint\":15,"
      "\"thema\":1,"
      "\"pointType\":0,"
      "\"times\":[201506,201507],"
      "\"monthData\":{"
      "\"time201506\":{\"day1\":0,\"day2\":1,\"day3\":2,\"day4\":3,\"day5\":0,\"day6\":1,\"day7\":2,\"day8\":3},"
      "\"time201507\":{\"day1\":2,\"day2\":2,\"day3\":2}"
      "}"
      "}"
      "],"
      "\"lastDispId\":0"
      "}";
   cocos2d::UserDefault *_userDef = cocos2d::UserDefault::getInstance();
   _userDef->setStringForKey("appData", jsonStr);

  _allData = PreferenceManager::load();
}

void ChecalModel::setDispId(int id) {
  _allData.dispId = id;
  PreferenceManager::save(_allData);

  ViewThemaModel::setThema(getThema());
  auto titleEvt = EventCustom(Events::UPDATE_TITLE);
  Director::getInstance()->getEventDispatcher()->dispatchEvent(&titleEvt);
  auto pointEvt = EventCustom(Events::UPDATE_POINT);
  Director::getInstance()->getEventDispatcher()->dispatchEvent(&pointEvt);
}

// これなんだっけ？なんでセーブしないんだ
void ChecalModel::setMonthData(int time, std::map<int, Stamp::Type> data) { getCalendarData()->monthDataList[time] = data; }
// これなんだっけ？なんでセーブしないんだ
void ChecalModel::setDayData(int time, int day, Stamp::Type stamp) { getCalendarData()->monthDataList[time][day] = stamp; }

void ChecalModel::setDayData(struct tm time, Stamp::Type stamp) {
  auto data = getCalendarData();
  auto originalPoint = getTotalPoint(time);  // もともとのポイント
  auto goalPoint = getGoalPoint();

  auto timeString = DateUtil::toDateId(time);
  data->monthDataList[timeString][time.tm_mday] = stamp;

  PreferenceManager::save(_allData);
  auto currentPoint = getTotalPoint(time);

  auto pointEvt = EventCustom(Events::UPDATE_POINT);
  Director::getInstance()->getEventDispatcher()->dispatchEvent(&pointEvt);

  // この更新で目標を達成した
  if (currentPoint == goalPoint && originalPoint < currentPoint) {
    auto pointEvt2 = EventCustom(Events::MISSION_COMPLETED);
    Director::getInstance()->getEventDispatcher()->dispatchEvent(&pointEvt2);
  }
}

void ChecalModel::setTitle(std::string title) {
  auto data = getCalendarData();
  data->title = title;

  PreferenceManager::save(_allData);
  auto evt = EventCustom(Events::UPDATE_TITLE);
  Director::getInstance()->getEventDispatcher()->dispatchEvent(&evt);
}
void ChecalModel::setGoalPoint(int goalPoint) {
  auto data = getCalendarData();
  data->goalPoint = goalPoint;

  PreferenceManager::save(_allData);
  auto pointEvt = EventCustom(Events::UPDATE_POINT);
  Director::getInstance()->getEventDispatcher()->dispatchEvent(&pointEvt);
}

void ChecalModel::setPointType(int pointType) {
  auto data = getCalendarData();
  data->pointType = pointType;

  PreferenceManager::save(_allData);
  auto pointEvt = EventCustom(Events::UPDATE_POINT);
  Director::getInstance()->getEventDispatcher()->dispatchEvent(&pointEvt);
}

void ChecalModel::setThema(ViewThemaModel::Type thema) {
  auto data = getCalendarData();
  data->thema = thema;

  ViewThemaModel::setThema(thema);
  PreferenceManager::save(_allData);
}

ViewThemaModel::Type ChecalModel::getThema() {
  if (_allData.dispId == -1) {
    return ViewThemaModel::Type::Red;
  }
  return getCalendarData(_allData.dispId)->thema;
}

std::string ChecalModel::getTitle() {
  if (_allData.dispId == -1) {
    return "";
  }
  return getCalendarData(_allData.dispId)->title;
}

std::vector<std::shared_ptr<ChecalModel::CalendarData>> ChecalModel::getChecalModel() { return _allData.calenderDataList; }

std::shared_ptr<ChecalModel::CalendarData> ChecalModel::getCalendarData(int id) {
  for (int i = 0; i < _allData.calenderDataList.size(); ++i) {
    if (_allData.calenderDataList[i]->id == id) {
      return _allData.calenderDataList[i];
    }
  }
  return nullptr;
}

std::shared_ptr<ChecalModel::CalendarData> ChecalModel::getCalendarData() {
  for (int i = 0; i < _allData.calenderDataList.size(); ++i) {
    if (_allData.calenderDataList[i]->id == _allData.dispId) {
      return _allData.calenderDataList[i];
    }
  }
  return nullptr;
}

ChecalModel::MonthData ChecalModel::getMonthData(struct tm time) {
  auto date = DateUtil::toDateId(time);
  return getCalendarData()->monthDataList[date];
}

Stamp::Type ChecalModel::getTodayStamp() {
  auto date = DateUtil::getCurrentTime();
  auto dateId = DateUtil::toDateId(date);
  auto stamp = getCalendarData(_allData.dispId)->monthDataList[dateId][date.tm_mday];
  return stamp;
}

void ChecalModel::createNewCalender() {
  int id = 1;
  if (_allData.calenderDataList.size() != 0) {
    id = _allData.calenderDataList[_allData.calenderDataList.size() - 1]->id + 1;
  }

  CalendarData hanaData;
  hanaData.id = id;
  hanaData.title = "新しい目標";
  hanaData.pointType = 0;
  hanaData.goalPoint = 10;
  hanaData.thema = static_cast<ViewThemaModel::Type>(arc4random() % 4);
  hanaData.monthDataList[201507][1] = Stamp::Type::NONE;

  auto data = std::make_shared<CalendarData>(hanaData);
  _allData.calenderDataList.push_back(data);

  PreferenceManager::save(_allData);

  setDispId(hanaData.id);
}

int ChecalModel::getTotalPoint(struct tm time) {
  auto date = DateUtil::toDateId(time);

  std::map<int, Stamp::Type> monthData;
  monthData = getCalendarData()->monthDataList[date];

  int totalPoint = 0;

  for (int i = 1; i <= 31; ++i) {
    if (monthData[i] != Stamp::Type::NONE) {
      if (getCalendarData()->pointType == 0) {
        totalPoint += 1;
      } else {
        if (monthData[i] == Stamp::Type::TRIANGLE) {
          totalPoint += 1;
        } else if (monthData[i] == Stamp::Type::HEXAGON) {
          totalPoint += 2;
        } else if (monthData[i] == Stamp::Type::STAR) {
          totalPoint += 3;
        }
      }
    }
  }

  return totalPoint;
}

int ChecalModel::getGoalPoint() { return getCalendarData()->goalPoint; }

void ChecalModel::deleteCalendarData(int id) {
  for (int i = 0; i < _allData.calenderDataList.size(); ++i) {
    if (_allData.calenderDataList[i]->id == _allData.dispId) {
      _allData.calenderDataList.erase(_allData.calenderDataList.begin() + i);
      _allData.dispId = -1;
      PreferenceManager::save(_allData);
      return;
    }
  }
}
