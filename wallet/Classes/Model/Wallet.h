#pragma once

#include "cocos2d.h"

class Wallet{
public:
    static Wallet* sharedWallet();
    Wallet();
    
    struct Item{
        int money;
        int category;
        std::string memo;
    };
    
    using DailyItems = std::vector<Item>;
    using MonthlyItems = std::map<int,DailyItems>;
    
    static MonthlyItems getMonthlyData(int monthID);
    static MonthlyItems getMonthlyData(struct tm time);
    
    static DailyItems getDailyData(int dayID);
    static DailyItems getDailyData(struct tm time);
    
private:
    static Wallet* walletSingleton;
    
    std::map<std::string,MonthlyItems> _allData;
};