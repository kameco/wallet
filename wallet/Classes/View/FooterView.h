#pragma once

#include "cocos2d.h"
#include "ui/CocosGUI.h"

class FooterView : public cocos2d::ui::Layout {
public:
  virtual bool init();
  CREATE_FUNC(FooterView);

private:
  cocos2d::Size _visibleSize;

  cocos2d::ui::Button *_addButton = nullptr;
  cocos2d::ui::Button *_categoryButton = nullptr;
  cocos2d::ui::Button *_calenderButton = nullptr;

  void createView();
  void addEvents();
};
