#pragma once

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "ChecalModel.h"

#include "ListCell.h"
#include "DayCellView.h"

class GraphView : public cocos2d::ui::Layout {
 public:
  virtual bool init();
  static GraphView* create();

  void open(bool effect);
  void close();

  ~GraphView();

 private:
  cocos2d::Size _visibleSize;
  ChecalModel* _appData;
  cocos2d::EventListenerTouchOneByOne* _listener = nullptr;
};
