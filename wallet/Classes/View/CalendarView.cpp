#include "CalendarView.h"
#include "DateUtil.h"
#include "Events.h"
#include "ViewThemaModel.h"

#include <time.h>

USING_NS_CC;

CalendarView *CalendarView::create(tm dispTime) {
  CalendarView *layout = new (std::nothrow) CalendarView();
  if (layout && layout->init(dispTime)) {
    layout->autorelease();
    return layout;
  }
  CC_SAFE_DELETE(layout);
  return nullptr;
}

bool CalendarView::init(tm dispTime) {
  if (!ui::Layout::init()) {
    return false;
  }

  _dispTime = dispTime;
  _visibleSize = Director::getInstance()->getVisibleSize();
  setContentSize(Size(_visibleSize.width - 80, 770));
  setAnchorPoint({0.5, 0.5});
  setPosition(_visibleSize / 2);

  createCalendarView();
  // addEvent();

  return true;
}

void CalendarView::createCalendarView() {
  std::vector<std::string> week = {"Sun", "Mon", "Tue", "Wed",
                                   "Thu", "Fri", "Sat"};
  std::vector<std::string> month = {
      "January", "February", "March",     "April",   "May",      "June",
      "July",    "August",   "September", "October", "November", "December"};

  int firstWeek = DateUtil::getFirstDayWeekday(_dispTime);
  auto lastDay = DateUtil::getLastDayTime(_dispTime);

  setCascadeOpacityEnabled(true);

  // setAnchorPoint(Vec2(0.5, 0.5 - 6));  // 0.5 -> -1.5 = -2
  // setPosition(Vec2(_visibleSize.width / 2, _visibleSize.height / 2 -
  // getContentSize().height * 6));

  auto monthLabel =
      ui::TextBMFont::create(month.at(_dispTime.tm_mon), "Font/test.fnt");
  monthLabel->setAnchorPoint(Vec2::ZERO);
  monthLabel->setColor(ViewThemaModel::getFontColor());
  monthLabel->setScale(0.8);
  monthLabel->setPosition(Vec2(0, 710));
  addChild(monthLabel);

  std::string yearText =
      cocos2d::StringUtils::toString(_dispTime.tm_year + 1900);
  //  if (!_isEnabled) {
  //    yearText = cocos2d::StringUtils::toString(dispTime.tm_year + 1900) + "."
  //    + cocos2d::StringUtils::toString(dispTime.tm_mon + 1);
  //  }
  auto yearLabel =
      ui::TextBMFont::create(yearText, "Font/sTest.fnt"); // numLight.fnt
  yearLabel->setAnchorPoint(Vec2::ZERO);
  yearLabel->setColor(ViewThemaModel::getFontColor());
  yearLabel->setScale(0.6);
  yearLabel->setPosition(Vec2(0, 685));
  addChild(yearLabel);

  _nextButton = ui::Button::create();
  _nextButton->loadTextureNormal("nextButtonNormal.png");
  _nextButton->loadTexturePressed("nextButtonPressed.png");
  _nextButton->setColor(ViewThemaModel::getMainColor());
  _nextButton->setFlippedX(true);
  _nextButton->setAnchorPoint(Vec2(0, 0));
  _nextButton->setPosition(Vec2(getContentSize().width + 15, 703));
  addChild(_nextButton);

  _backButton = ui::Button::create();
  _backButton->loadTextureNormal("nextButtonNormal.png");
  _backButton->loadTexturePressed("nextButtonPressed.png");
  _backButton->setColor(ViewThemaModel::getMainColor());
  _backButton->setAnchorPoint(Vec2(1, 0));
  _backButton->setPosition(Vec2(getContentSize().width + 15 - 80, 703));
  addChild(_backButton);

  cocos2d::Vector<ui::TextBMFont *> weekTexts;
  auto cellSize = (getContentSize().width + 20) / 7;
  for (int i = 0; i < (int)week.size(); i++) {
    auto text = ui::TextBMFont::create(week.at(i), "Font/test.fnt");
    text->setColor(Color3B(142, 142, 142));
    text->setScale(0.25);
    text->setOpacity(255 * 0.4);
    text->setPosition(Vec2(cellSize * i + cellSize / 2 - 10, 620));
    addChild(text);
    weekTexts.pushBack(text);
  }

  cocos2d::Vector<DayCellView *> dayCells;
  struct tm dayTime = _dispTime;
  dayTime.tm_mday = 1;

  auto size = (_visibleSize.width - 80 + 20) / 7;
  for (int i = 0; i < 6; i++) {
    auto weekLineView = WeekLineView::create(_dispTime, i);
    addChild(weekLineView);
  }
}

void CalendarView::targetingDayCell(cocos2d::Touch *touch) {
  touch->getLocation();
  Vec2 pos = Vec2(0, 0);
  for (auto dayCell : _dayCells) {
    Vec2 localPos = dayCell->convertToWorldSpace(pos);
    auto rect = dayCell->getBoundingBox();
    rect.origin = localPos;
    if (rect.containsPoint(touch->getLocation()) &&
        dayCell->getCellState() == DayCellView::CellState::ACTIVE) {
      targetingDayCell(dayCell);
    } else if (rect.containsPoint(touch->getLocation()) &&
               dayCell->getCellState() == DayCellView::CellState::INACTIVE) {
      dayCell->runDisableAction();
    }
  }
}

void CalendarView::targetingDayCell(int day) {
  for (int i = 0; i < _dayCells.size(); ++i) {
    if (_dayCells.at(i)->getIsToday()) {
      targetingDayCell(_dayCells.at(i));
      return;
    }
  }
}

void CalendarView::targetingDayCell(DayCellView *dayCell) {
  targetingDayCell(false);
  _selectDayCell = dayCell;
}

void CalendarView::targetingDayCell(bool target) {
  if (_selectDayCell && !target) {
    _selectDayCell->isSelect(false);
    _selectDayCell = nullptr;
  }
}

void CalendarView::addEvent() {
  _backButton->addTouchEventListener(
      [this](Ref *pSender, cocos2d::ui::Widget::TouchEventType type) {
        if (type == cocos2d::ui::Widget::TouchEventType::ENDED) {
          CCLOG("_backButton touch ended.");
          auto evt = EventCustom(Events::SELECTED_MOVE_CALENDER_PAGE);
          int data = -1;
          evt.setUserData(&data);
          getEventDispatcher()->dispatchEvent(&evt);
        }
      });

  _nextButton->addTouchEventListener(
      [this](Ref *pSender, cocos2d::ui::Widget::TouchEventType type) {
        if (type == cocos2d::ui::Widget::TouchEventType::ENDED) {
          CCLOG("_nextButton touch ended.");
          auto evt = EventCustom(Events::SELECTED_MOVE_CALENDER_PAGE);
          int data = 1;
          evt.setUserData(&data);
          getEventDispatcher()->dispatchEvent(&evt);
        }
      });

  //画面全体
  //  auto listener = EventListenerTouchOneByOne::create();
  //  listener->setSwallowTouches(true);
  //
  //  listener->onTouchBegan = CC_CALLBACK_2(CalendarView::onTouchBegan, this);
  //  listener->onTouchMoved = CC_CALLBACK_2(CalendarView::onTouchMoved, this);
  //  listener->onTouchEnded = CC_CALLBACK_2(CalendarView::onTouchEnded, this);
  //
  //  auto dip = Director::getInstance()->getEventDispatcher();
  //  dip->addEventListenerWithSceneGraphPriority(listener, this);
  //  dip->setPriority(listener, -1);
}

bool CalendarView::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event) {
  Vec2 pos = Vec2(0, 0);

  _touchBeganPos = touch->getLocation();

  targetingDayCell(false);

  return true;
}

void CalendarView::onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *event) {
  // タッチ中の処理
  Vec2 touchMovedPos = touch->getLocation();
  float xDiff = touchMovedPos.x - _touchBeganPos.x;
  float rotate = xDiff * 0.005;
  if (rotate < 0) {
    rotate = 360 + rotate;
  }
  setRotation(rotate);
}

void CalendarView::onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event) {
  // タッチが終わった時の処理
  Vec2 touchEndedPos = touch->getLocation();

  //一定より横移動していたら、ページング
  if (touchEndedPos.x > _touchBeganPos.x + 80) {
    auto evt = EventCustom(Events::SELECTED_MOVE_CALENDER_PAGE);
    int data = -1;
    evt.setUserData(&data);
    getEventDispatcher()->dispatchEvent(&evt);
  } else if (touchEndedPos.x < _touchBeganPos.x - 80) {
    auto evt = EventCustom(Events::SELECTED_MOVE_CALENDER_PAGE);
    int data = 1;
    evt.setUserData(&data);
    getEventDispatcher()->dispatchEvent(&evt);
  } else {
    auto rotateAct = RotateTo::create(0.1, 0);
    auto easeAct = EaseQuadraticActionInOut::create(rotateAct);
    runAction(easeAct);

    //カレンダーのセルをクリック
    targetingDayCell(touch);
  }
}

CalendarView::~CalendarView() {
  for (auto listner : _listners) {
    getEventDispatcher()->removeEventListener(listner);
  }
}
