#pragma once

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "ChecalModel.h"

#include "DayCellView.h"

class ListCell : public cocos2d::ui::Button {
 public:
  bool init(std::shared_ptr<ChecalModel::CalendarData> danaData);
  bool init();
  static ListCell* create(std::shared_ptr<ChecalModel::CalendarData> danaData);
  static ListCell* create();
  void touchEffect(Ref* pSender, cocos2d::ui::Widget::TouchEventType type);
  void addCalenderEf();
  void setHighlighted(bool highlighted);

 protected:
 private:
  cocos2d::Size _visibleSize;

  int _id = 0;

  cocos2d::ui::Text* _text = nullptr;
  cocos2d::ui::ImageView* _stamp = nullptr;
  cocos2d::ui::ImageView* _inlineStamp = nullptr;
};
