#pragma once

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "ChecalModel.h"

#include "DayCellView.h"

class SettingView : public cocos2d::ui::Layout,
                    public cocos2d::ui::EditBoxDelegate {
 public:
  static SettingView* create();

  void open();
  void close();
  void updateData();

 private:
  bool init();
  ChecalModel* _appData;
  void setThema();

  cocos2d::Size _visibleSize;
  cocos2d::ui::Button* _settingButton = nullptr;
  cocos2d::ui::Button* _closeButton = nullptr;
  cocos2d::ui::EditBox* _todoEditBox = nullptr;
  cocos2d::ui::EditBox* _goalPointEditBox = nullptr;
  cocos2d::ui::ImageView* _colorSelected = nullptr;
  std::vector<cocos2d::ui::Button*> _colorTips;
  int _id = 0;

  virtual void editBoxEditingDidBegin(cocos2d::ui::EditBox* editBox);
  virtual void editBoxEditingDidEnd(cocos2d::ui::EditBox* editBox);
  virtual void editBoxTextChanged(cocos2d::ui::EditBox* editBox,
                                  const std::string& text);
  virtual void editBoxReturn(cocos2d::ui::EditBox* editBox);
};
