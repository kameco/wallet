#pragma once

#include "Wallet.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"

class ItemCellView : public cocos2d::ui::Widget {
public:
  static ItemCellView *create(Wallet::Item item);
  ~ItemCellView();

private:
  bool init(Wallet::Item item);
  cocos2d::Size _visibleSize;
};
