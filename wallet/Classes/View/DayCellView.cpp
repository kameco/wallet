#include "DayCellView.h"
#include "ViewThemaModel.h"

#include "ChecalModel.h"

#include "ActionUtil.h"
#include "DateUtil.h"
#include "Events.h"

#include <time.h>

USING_NS_CC;

DayCellView *DayCellView::create(tm dispTime) {
  DayCellView *layout = new (std::nothrow) DayCellView();
  if (layout && layout->init(dispTime)) {
    layout->autorelease();
    return layout;
  }
  CC_SAFE_DELETE(layout);
  return nullptr;
}

bool DayCellView::init() {
  if (!ui::Widget::init()) {
    return false;
  }

  //  int rand = arc4random() % 3 + 1;
  //  auto stampType = static_cast<Stamp::Type>(rand);
  //
  //  _visibleSize = Director::getInstance()->getVisibleSize();
  //  auto size = (_visibleSize.width - 80 + 20) / 7;
  //  setContentSize(Size(size, size));
  //
  //  setStampType(stampType, false);
  //  setCellState(CellState::INVISIBLE);
  //
  //  setOpacity(255 * 0.15);

  return true;
}

bool DayCellView::init(tm dispTime) {
  if (!ui::Widget::init()) {
    return false;
  }

  setTouchEnabled(true);
  addTouchEventListener(
      [this](Ref *pSender, cocos2d::ui::Widget::TouchEventType type) {
        if (type == cocos2d::ui::Widget::TouchEventType::ENDED) {
          CCLOG("dayCell touch ended. %i",
                DateUtil::getWeekCountOfMonth(_dispTime));

          auto evt = EventCustom(Events::SELECTED_DAY_CELL);
          evt.setUserData(&_dispTime);
          getEventDispatcher()->dispatchEvent(&evt);
        }
      });

  _dispTime = dispTime;

  _visibleSize = Director::getInstance()->getVisibleSize();

  auto size = (_visibleSize.width - 80 + 20) / 7;
  setContentSize(Size(size, size));

  auto nowTime = DateUtil::getCurrentTime();

  _dayLabel = ui::TextBMFont::create(
      cocos2d::StringUtils::toString(dispTime.tm_mday), "Font/numLight.fnt");
  _dayLabel->setAnchorPoint(Vec2(0.5, 0.5));
  _dayLabel->setColor(ViewThemaModel::getFontColor());
  _dayLabel->setScale(0.6);
  _dayLabel->setPosition(getContentSize() / 2);
  addChild(_dayLabel, 10);

  if (nowTime.tm_mday == dispTime.tm_mday &&
      nowTime.tm_year == dispTime.tm_year &&
      nowTime.tm_mon == dispTime.tm_mon) {
    _dayLabel->setColor(ViewThemaModel::getMainColor());
    _dayLabel->setFntFile("Font/numBold.fnt");
    _isToday = true;
    setCellState(CellState::ACTIVE);
  } else {
    _dayLabel->setColor(ViewThemaModel::getFontColor());
    _isToday = false;
    setCellState(CellState::ACTIVE);

    if (DateUtil::toDateId(nowTime) < DateUtil::toDateId(dispTime)) {
      setOpacity(255 * 0.3);
      setCellState(CellState::INACTIVE);
    } else if (DateUtil::toDateId(nowTime) == DateUtil::toDateId(dispTime)) {
      if (nowTime.tm_mday < dispTime.tm_mday) {
        setOpacity(255 * 0.3);
        setCellState(CellState::INACTIVE);
      }
    }
  }

  _changeThemaListner = getEventDispatcher()->addCustomEventListener(
      Events::UPDATE_THEMA, [this](EventCustom *event) { setThema(); });

  return true;
}

void DayCellView::runDisableAction() {
  auto basePosition = _dayLabel->getPosition();
  auto move1 = MoveTo::create(0.05, basePosition + Vec2(10, 0));
  auto move2 = MoveTo::create(0.05, basePosition + Vec2(-10, 0));
  auto move3 = MoveTo::create(0.04, basePosition + Vec2(6, 0));
  auto move4 = MoveTo::create(0.04, basePosition + Vec2(-6, 0));
  auto move5 = MoveTo::create(0.03, basePosition + Vec2(3, 0));
  auto move6 = MoveTo::create(0.03, basePosition + Vec2(-3, 0));
  auto move7 = MoveTo::create(0.02, basePosition);
  auto seq1 =
      Sequence::create(move1, move2, move3, move4, move5, move6, move7, NULL);
  _dayLabel->runAction(seq1);
}

void DayCellView::setThema() {
  if (_selectEf) {
    auto color = TintTo::create(0.2, ViewThemaModel::getStampColor());
    auto ease = EaseQuadraticActionOut::create(color);
    _selectEf->runAction(ease);
  }

  if (_isToday) {
    auto color = TintTo::create(0.2, ViewThemaModel::getMainColor());
    auto ease = EaseQuadraticActionOut::create(color);
    _dayLabel->runAction(ease);
    //_dayLabel->setColor(ViewThemaModel::getMainColor());
  } else {
    auto color = TintTo::create(0.2, ViewThemaModel::getFontColor());
    auto ease = EaseQuadraticActionOut::create(color);
    _dayLabel->setColor(ViewThemaModel::getFontColor());
  }
}

void DayCellView::isSelect(bool select) {
  if (select) {
    if (!_selectEf) {
      _selectEf = ui::ImageView::create("selectDayEf.png");
      _selectEf->setAnchorPoint(Vec2(0.5, 0.5));
      _selectEf->setPosition(Vec2(getContentSize().width / 2 + 1,
                                  getContentSize().height / 2 + 2));
      _selectEf->setColor(ViewThemaModel::getStampColor());
      _selectEf->setOpacity(255 * 0.8);
      addChild(_selectEf);
      _selectEf->runAction(RepeatRotate::create());
    }
  } else {
    if (_selectEf) {
      _selectEf->removeFromParentAndCleanup(true);
      _selectEf = nullptr;
    }
  }
}

DayCellView::~DayCellView() {
  getEventDispatcher()->removeEventListener(_changeThemaListner);
}
