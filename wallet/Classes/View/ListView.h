#pragma once

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "ChecalModel.h"

#include "ListCell.h"
#include "DayCellView.h"

class ListView : public cocos2d::ui::Layout {
 public:
  virtual bool init();
  static ListView* create();

  void open();
  void close();
  void initList();

  ~ListView();
  cocos2d::EventListenerCustom* _changeThemaListner = nullptr;

 private:
  ChecalModel* _appData;
  ListCell* _pulsList = nullptr;

  cocos2d::Size _visibleSize;
  cocos2d::ui::Button* _closeButton = nullptr;
  int _id = 0;

  void setThema();
};
