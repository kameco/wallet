#pragma once

#include "cocos2d.h"
#include "ui/CocosGUI.h"

class TotalFeeView : public cocos2d::ui::Layout {
public:
    virtual bool init();
    CREATE_FUNC(TotalFeeView);
    
private:
    cocos2d::Size _visibleSize;
};
