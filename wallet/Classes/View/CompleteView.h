#pragma once

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "ChecalModel.h"

#include "ListCell.h"
#include "DayCellView.h"

class CompleteView : public cocos2d::ui::Layout {
 public:
  virtual bool init();
  static CompleteView* create();

  void open();
  void close();

  ~CompleteView();

 private:
  cocos2d::Size _visibleSize;
  ChecalModel* _appData;
  cocos2d::EventListenerTouchOneByOne* _listener = nullptr;
  bool _touchEnabled = false;

  cocos2d::Sprite* _titleSprite = nullptr;
  cocos2d::Sprite* _bg1Sprite = nullptr;
  cocos2d::Sprite* _bg2Sprite = nullptr;
  cocos2d::ui::TextBMFont* _dayLabel = nullptr;
};
