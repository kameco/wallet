#include "CompleteView.h"
#include "ViewThemaModel.h"
#include "Events.h"
#include "ActionUtil.h"
#include "ChecalModel.h"
USING_NS_CC;

CompleteView* CompleteView::create() {
  CompleteView* layout = new (std::nothrow) CompleteView();
  if (layout && layout->init()) {
    layout->autorelease();
    return layout;
  }
  CC_SAFE_DELETE(layout);
  return nullptr;
}

bool CompleteView::init() {
  if (!ui::Layout::init()) {
    return false;
  }

  _bg1Sprite = Sprite::create("completeBg1.png");
  _bg1Sprite->setOpacity(255 * 0.9);
  _bg1Sprite->setColor(ViewThemaModel::getMainColor());
  _bg1Sprite->setRotation(30);
  addChild(_bg1Sprite);
  _bg1Sprite->runAction(RepeatRotate::create(10));
  _bg2Sprite = Sprite::create("completeBg2.png");
  _bg2Sprite->setOpacity(255 * 0.9);
  _bg2Sprite->setColor(ViewThemaModel::getMainColor());
  addChild(_bg2Sprite);
  _bg2Sprite->runAction(RepeatRotate::create(-10));
  _titleSprite = Sprite::create("completeTitle.png");
  _titleSprite->setPositionY(15);
  addChild(_titleSprite);
  setContentSize(_bg2Sprite->getContentSize());

  auto appData = ChecalModel::sharedChecalModel();
  auto goalPoint = appData->getGoalPoint();

  _dayLabel = ui::TextBMFont::create(StringUtils::toString(goalPoint) + "DAYS", "Font/test.fnt");
  _dayLabel->setScale(0.4);
  _dayLabel->setPositionY(-85);
  addChild(_dayLabel);

  auto visibleSize = Director::getInstance()->getVisibleSize();
  setPosition(visibleSize / 2);

  setScale(0.0);
  open();

  //画面全体
  auto listener = EventListenerTouchOneByOne::create();
  listener->setSwallowTouches(true);

  listener->onTouchBegan = [](Touch* touch, Event* event) { return true; };
  listener->onTouchMoved = [](Touch* touch, Event* event) {};
  listener->onTouchEnded = [this](Touch* touch, Event* event) {
    if (_touchEnabled) {
      close();
    }
  };

  auto dip = Director::getInstance()->getEventDispatcher();
  dip->addEventListenerWithSceneGraphPriority(listener, this);
  dip->setPriority(listener, -1);

  return true;
}

void CompleteView::open() {
  auto delay1 = DelayTime::create(0);
  auto scale1 = ScaleTo::create(0.2, 1.25);
  auto rotate1 = RotateTo::create(0.2, 0);
  auto spawn1 = Spawn::create(scale1, rotate1, NULL);
  auto ease1 = EaseQuadraticActionIn::create(spawn1);

  auto func = CallFunc::create([this]() {
    auto efSprite = Sprite::create("completeEf.png");
    efSprite->setOpacity(255 * 0.9);
    efSprite->setScale(2.0f);
    efSprite->setColor(ViewThemaModel::getStampColor());
    addChild(efSprite);

    auto delay = DelayTime::create(0.2);
    auto fade = FadeOut::create(0.3);
    auto remove = RemoveSelf::create();
    auto seq = Sequence::create(delay, fade, remove, NULL);
    efSprite->runAction(seq);

    _touchEnabled = true;
  });

  auto scale2 = ScaleTo::create(0.05, 1.0);
  auto ease2 = EaseQuadraticActionOut::create(scale2);
  auto seq1 = Sequence::create(delay1, ease1, func, ease2, NULL);
  runAction(seq1);
}

void CompleteView::close() {
  auto fade = FadeOut::create(0.2);
  auto ease = EaseQuadraticActionOut::create(fade);
  auto func = CallFunc::create([this]() {
    auto pointEvt = EventCustom(Events::CLOSED_MISSION_COMPLETED_EFFECT);
    Director::getInstance()->getEventDispatcher()->dispatchEvent(&pointEvt);
  });
  auto remove = RemoveSelf::create();
  auto spawn = Spawn::create(ease, func, remove, NULL);
  runAction(spawn);
}

CompleteView::~CompleteView() {
  // getEventDispatcher()->removeEventListener(_changeThemaListner);
}
