#include "TotalFeeView.h"
#include "ViewThemaModel.h"
#include "Events.h"

USING_NS_CC;

bool TotalFeeView::init() {
    if (!ui::Layout::init()) {
        return false;
    }
    
    _visibleSize = Director::getInstance()->getVisibleSize();
    setContentSize(Size(_visibleSize.width, 50));
    setAnchorPoint(Vec2(0, 0));
    // setPosition(Vec2(0, _visibleSize.height));
    
    setBackGroundColorType(BackGroundColorType::SOLID);
    setBackGroundColor(Color3B(10, 10, 10));
    
    auto _currentMonthlabel = ui::TextBMFont::create("Monthly Total", "Font/sTest.fnt");
    _currentMonthlabel->setAnchorPoint(Vec2(0, 0));
    _currentMonthlabel->setColor(Color3B(255, 255, 255));
    _currentMonthlabel->setScale(0.7);
    _currentMonthlabel->setPosition(Vec2(10, 0));
    addChild(_currentMonthlabel, 10);
    
    auto _totalLabel = ui::TextBMFont::create("999,999,999", "Font/numLight.fnt");
    _totalLabel->setAnchorPoint(Vec2(1, 0));
    _totalLabel->setColor(Color3B(190, 190, 190));
    _totalLabel->setScale(0.6);
    _totalLabel->setPosition(Vec2(_visibleSize.width-10, 10));
    addChild(_totalLabel, 10);
    
    auto addButton = Sprite::create("addButton.png");
    addButton->setPosition(_visibleSize.width / 2, 100);
//    addButton->setColor(Color3B(243, 141, 137));
    addButton->setScale(0.9);
    addChild(addButton);
    
    return true;
}
