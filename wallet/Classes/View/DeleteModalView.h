#pragma once

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "DayCellView.h"
#include "ChecalModel.h"

class DeleteModalView : public cocos2d::ui::Layout {
 public:
  virtual bool init(const std::shared_ptr<ChecalModel::CalendarData>);
  ~DeleteModalView();
  static DeleteModalView* create(const std::shared_ptr<ChecalModel::CalendarData>);
  void open();
  void close();

  CC_SYNTHESIZE(DayCellView*, _selectDayCell, SelectDayCell);
  // CREATE_FUNC(DeleteModalView);

 private:
  cocos2d::Size _visibleSize;
  cocos2d::ui::ImageView* _flame = nullptr;
  cocos2d::ui::ImageView* _flameShadow = nullptr;

  // cocos2d::Vector<cocos2d::ui::Button*> _stamps;
  cocos2d::Map<int, cocos2d::ui::Button*> _stamps;

  cocos2d::ui::Button* _stamp0 = nullptr;
  cocos2d::ui::Button* _stamp1 = nullptr;
  cocos2d::ui::Button* _stamp2 = nullptr;
  cocos2d::ui::Button* _stamp3 = nullptr;

  cocos2d::EventListenerTouchOneByOne* _listener = nullptr;

  void addEvent();
};
