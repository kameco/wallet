#include "Events.h"
#include "FooterView.h"
#include "ViewThemaModel.h"

USING_NS_CC;

bool FooterView::init() {
  if (!ui::Layout::init()) {
    return false;
  }

  createView();
  addEvents();

  //    cocos2d::ui::Button *_addButton = nullptr;
  //    cocos2d::ui::Button *_categoryButton = nullptr;
  //    cocos2d::ui::Button *_calenderButton = nullptr;

  return true;
}

void FooterView::addEvents() {
  _calenderButton->addTouchEventListener(
      [this](Ref *pSender, cocos2d::ui::Widget::TouchEventType type) {
        if (type == cocos2d::ui::Widget::TouchEventType::ENDED) {
          auto evt = EventCustom(Events::TOUCHED_CALENDER_BUTTON);
          getEventDispatcher()->dispatchEvent(&evt);
        }
      });
}

void FooterView::createView() {

  _visibleSize = Director::getInstance()->getVisibleSize();
  setContentSize(Size(_visibleSize.width, 140));
  setAnchorPoint(Vec2(0, 0));

  setBackGroundImage("footerBg.png");

  _calenderButton = ui::Button::create("iconCel.png");
  _calenderButton->setPosition(Vec2(_visibleSize.width / 5, 90));
  _calenderButton->setScale(0.8);
  addChild(_calenderButton);

  _categoryButton = ui::Button::create("iconCate.png");
  _categoryButton->setPosition(Vec2(_visibleSize.width / 5 * 4, 90));
  _categoryButton->setScale(0.8);
  addChild(_categoryButton);

  auto totalBase = ui::Layout::create();
  totalBase->setContentSize(Size(_visibleSize.width, 50));
  totalBase->setBackGroundColorType(BackGroundColorType::SOLID);
  totalBase->setBackGroundColor(Color3B(10, 10, 10));
  addChild(totalBase);

  auto _currentMonthlabel =
      ui::TextBMFont::create("Monthly Total", "Font/sTest.fnt");
  _currentMonthlabel->setAnchorPoint(Vec2(0, 0));
  _currentMonthlabel->setColor(Color3B(255, 255, 255));
  _currentMonthlabel->setScale(0.7);
  _currentMonthlabel->setPosition(Vec2(10, 0));
  addChild(_currentMonthlabel);

  auto _totalLabel = ui::TextBMFont::create("999,999,999", "Font/numLight.fnt");
  _totalLabel->setAnchorPoint(Vec2(1, 0));
  _totalLabel->setColor(Color3B(190, 190, 190));
  _totalLabel->setScale(0.6);
  _totalLabel->setPosition(Vec2(_visibleSize.width - 10, 10));
  addChild(_totalLabel);

  _addButton = ui::Button::create("addButton.png");
  _addButton->setPosition(Vec2(_visibleSize.width / 2, 100));
  _addButton->setScale(0.9);
  addChild(_addButton);
}
