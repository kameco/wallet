#include "GraphView.h"
#include "ViewThemaModel.h"
#include "Events.h"
#include "ActionUtil.h"
#include "DateUtil.h"
USING_NS_CC;

GraphView* GraphView::create() {
  GraphView* layout = new (std::nothrow) GraphView();
  if (layout && layout->init()) {
    layout->autorelease();
    return layout;
  }
  CC_SAFE_DELETE(layout);
  return nullptr;
}

bool GraphView::init() {
  if (!ui::Layout::init()) {
    return false;
  }

  setCascadeOpacityEnabled(true);
  setOpacity(0);

  _appData = ChecalModel::sharedChecalModel();  //うーんデータの取り扱いがたるい
  _visibleSize = Director::getInstance()->getVisibleSize();
  setContentSize(Size(_visibleSize.width - 100, 600));
  setAnchorPoint({0.5, 0.6});
  setPosition(_visibleSize / 2);

  auto posY = 0;

  auto cancelButton = ui::Button::create();
  cancelButton->loadTextureNormal("pointFlame.png");
  // cancelButton->loadTexturePressed("closeButtonPressed.png");
  //_closeButton->setColor(ViewThemaModel::getBackgroundColor());
  //_closeButton->setFlippedX(true);
  cancelButton->setAnchorPoint(Vec2(0.5, 0.5));  // 115
  cancelButton->setPosition(Vec2(getContentSize().width / 2, posY));
  cancelButton->setZoomScale(0);
  cancelButton->setColor(ViewThemaModel::getMainColor());
  addChild(cancelButton);
  auto cancelLabel = ui::TextBMFont::create("Close", "Font/test.fnt");
  cancelLabel->setAnchorPoint({0.5, 0.6});
  cancelLabel->setColor(ViewThemaModel::getMainColor());
  cancelLabel->setScale(0.4);
  cancelLabel->setPosition(cancelButton->getContentSize() / 2);
  cancelButton->addChild(cancelLabel);
  cancelButton->setCascadeOpacityEnabled(true);
  cancelButton->addTouchEventListener([=](Ref* pSender, cocos2d::ui::Widget::TouchEventType type) {
    if (type == cocos2d::ui::Widget::TouchEventType::BEGAN) {
      cancelButton->setOpacity(255 * 0.5);
    } else if (type == cocos2d::ui::Widget::TouchEventType::ENDED) {
      cancelButton->setOpacity(255);
      auto evt = EventCustom(Events::TOUCHED_CLOSE_GRAPH_BUTTON);
      getEventDispatcher()->dispatchEvent(&evt);
    } else if (type == cocos2d::ui::Widget::TouchEventType::CANCELED) {
      cancelButton->setOpacity(255);
    }
  });

  posY += 80;
  //  auto monthLabel = ui::TextBMFont::create("month", "Font/label.fnt");
  //  monthLabel->setColor(ViewThemaModel::getFontColor());
  //  monthLabel->setAnchorPoint(Vec2(0, 0));
  //  monthLabel->setScale(0.33);
  //  monthLabel->setPosition(Vec2(0, posY));
  //  // monthLabel->setOpacity(255 * 0.3);
  //  addChild(monthLabel);
  //
  //  posY += 50;
  auto CurrentTime = DateUtil::getCurrentTime();
  auto nowMonth = CurrentTime.tm_mon + 1;
  auto month = nowMonth;

  for (int i = 0; i < 13; ++i) {
    auto label = ui::TextBMFont::create(cocos2d::StringUtils::toString(month), "Font/numLight.fnt");
    label->setAnchorPoint(Vec2(0.5, 0));
    label->setColor(ViewThemaModel::getFontColor());
    label->setScale(0.6);
    label->setPosition(Vec2(getContentSize().width / 12 * i, posY));
    addChild(label);

    if (i != 0 && month == nowMonth) {
      label->setColor(ViewThemaModel::getMainColor());
      label->setFntFile("Font/numBold.fnt");
    }
    month = (month + 1 == 13) ? 1 : month + 1;
  }

  posY += 50;
  auto graphBase = DrawNode::create();
  graphBase->setCascadeOpacityEnabled(true);
  auto glayColor = Color4F(ViewThemaModel::getFontColor());
  glayColor.a = 0.3;
  // 450が30。
  graphBase->drawSegment(Vec2(0, posY), Vec2(getContentSize().width, posY), 1.0f, Color4F(ViewThemaModel::getFontColor()));
  graphBase->drawSegment(Vec2(0, 150 + posY), Vec2(getContentSize().width, 150 + posY), 1.0f, glayColor);
  graphBase->drawSegment(Vec2(0, 300 + posY), Vec2(getContentSize().width, 300 + posY), 1.0f, glayColor);
  graphBase->drawSegment(Vec2(0, 450 + posY), Vec2(getContentSize().width, 450 + posY), 1.0f, glayColor);

  auto graphLabel10 = ui::TextBMFont::create(cocos2d::StringUtils::toString(10), "Font/numLight.fnt");
  graphLabel10->setAnchorPoint(Vec2(1, 0.5));
  graphLabel10->setColor(ViewThemaModel::getFontColor());
  graphLabel10->setScale(0.6);
  graphLabel10->setPosition(Vec2(0, 150 + posY));
  addChild(graphLabel10);

  auto graphLabel20 = ui::TextBMFont::create(cocos2d::StringUtils::toString(20), "Font/numLight.fnt");
  graphLabel20->setAnchorPoint(Vec2(1, 0.5));
  graphLabel20->setColor(ViewThemaModel::getFontColor());
  graphLabel20->setScale(0.6);
  graphLabel20->setPosition(Vec2(0, 300 + posY));
  addChild(graphLabel20);

  auto graphLabel30 = ui::TextBMFont::create(cocos2d::StringUtils::toString(30), "Font/numLight.fnt");
  graphLabel30->setAnchorPoint(Vec2(1, 0.5));
  graphLabel30->setColor(ViewThemaModel::getFontColor());
  graphLabel30->setScale(0.6);
  graphLabel30->setPosition(Vec2(0, 450 + posY));
  addChild(graphLabel30);

  auto getGoalH = _appData->getGoalPoint() * 450 / 30;
  auto goalColor = Color4F(ViewThemaModel::getMainColor());
  goalColor.a = 0.3;
  graphBase->drawSegment(Vec2(0, getGoalH + posY), Vec2(getContentSize().width, getGoalH + posY), 1.0f, goalColor);

  addChild(graphBase);
  // drawSegment(スタートポイント,エンドポイント,太さ,色);

  auto distTime = DateUtil::moveMonth(CurrentTime, -11);
  auto pointH = 0;

  auto graph = DrawNode::create();
  graph->setCascadeOpacityEnabled(true);
  for (int i = 0; i < 13; ++i) {
    //    auto label2 = ui::TextBMFont::create(cocos2d::StringUtils::toString(_appData->getTotalPoint(distTime)), "Font/numLight.fnt");
    //    label2->setAnchorPoint(Vec2(0.5, 0));
    //    label2->setColor(ViewThemaModel::getMainColor());
    //    label2->setScale(0.6);
    //    label2->setPosition(Vec2(getContentSize().width / 12 * i, posY));
    //    addChild(label2);

    if (i == 0) {
      pointH = _appData->getTotalPoint(distTime) * 450 / 30;
      continue;
    }

    auto h = _appData->getTotalPoint(distTime) * 450 / 30;
    graphBase->drawSegment(Vec2(getContentSize().width / 12 * (i - 1), pointH + posY), Vec2(getContentSize().width / 12 * i, h + posY), 1.0f,
                           Color4F(ViewThemaModel::getMainColor()));
    pointH = h;

    distTime = DateUtil::moveMonth(distTime, 1);
  }
  addChild(graph);

  auto titleLabel = ui::TextBMFont::create("graph", "Font/test.fnt");
  titleLabel->setColor(ViewThemaModel::getFontColor());
  titleLabel->setAnchorPoint(Vec2(0, 0));
  titleLabel->setScale(0.8);
  titleLabel->setPosition(Vec2(-20, 500 + posY));
  addChild(titleLabel);

  _listener = EventListenerTouchOneByOne::create();
  _listener->setSwallowTouches(true);
  _listener->onTouchBegan = [](Touch* touch, Event* event) -> bool { return true; };
  auto dispatcher = Director::getInstance()->getEventDispatcher();
  dispatcher->addEventListenerWithSceneGraphPriority(_listener, this);

  return true;
}

void GraphView::open(bool effect) {
  float op = (effect) ? 0.15 : 1.0;
  auto fade = FadeTo::create(0.2, 255 * op);
  auto ease = EaseQuadraticActionOut::create(fade);
  runAction(ease);
}

void GraphView::close() {
  auto fade = FadeOut::create(0.2);
  auto ease = EaseQuadraticActionOut::create(fade);
  auto remove = RemoveSelf::create();
  auto spawn = Spawn::create(ease, remove, NULL);
  runAction(spawn);
}

GraphView::~GraphView() {
  // getEventDispatcher()->removeEventListener(_changeThemaListner);
}
