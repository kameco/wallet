#include "ItemCellView.h"
#include "ViewThemaModel.h"

#include "ChecalModel.h"

#include "ActionUtil.h"
#include "DateUtil.h"
#include "Events.h"

#include <time.h>

USING_NS_CC;

ItemCellView *ItemCellView::create(Wallet::Item item) {
  ItemCellView *layout = new (std::nothrow) ItemCellView();
  if (layout && layout->init(item)) {
    layout->autorelease();
    return layout;
  }
  CC_SAFE_DELETE(layout);
  return nullptr;
}

bool ItemCellView::init(Wallet::Item item) {
  if (!ui::Widget::init()) {
    return false;
  }

  setTouchEnabled(true);
  addTouchEventListener(
      [this](Ref *pSender, cocos2d::ui::Widget::TouchEventType type) {
        if (type == cocos2d::ui::Widget::TouchEventType::ENDED) {
          CCLOG("itemCell touch ended.");
          //          auto evt =
          //          EventCustom(Events::SELECTED_MOVE_CALENDER_PAGE);
          //          int data = -1;
          //          evt.setUserData(&data);
          //          getEventDispatcher()->dispatchEvent(&evt);
        }
      });

  _visibleSize = Director::getInstance()->getVisibleSize();
  setContentSize(Size(_visibleSize.width, 100));

  return true;
}

ItemCellView::~ItemCellView() {
  // getEventDispatcher()->removeEventListener(_changeThemaListner);
}
