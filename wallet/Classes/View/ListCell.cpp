#include "ListCell.h"
#include "ViewThemaModel.h"
#include "ActionUtil.h"

#include <time.h>

USING_NS_CC;

ListCell* ListCell::create(std::shared_ptr<ChecalModel::CalendarData> danaData) {
  ListCell* layout = new (std::nothrow) ListCell();
  if (layout && layout->init(danaData)) {
    layout->autorelease();
    return layout;
  }
  CC_SAFE_DELETE(layout);
  return nullptr;
}

ListCell* ListCell::create() {
  ListCell* layout = new (std::nothrow) ListCell();
  if (layout && layout->init()) {
    layout->autorelease();
    return layout;
  }
  CC_SAFE_DELETE(layout);
  return nullptr;
}

bool ListCell::init(std::shared_ptr<ChecalModel::CalendarData> hanaData) {
  if (!ui::Button::init()) {
    return false;
  }

  _visibleSize = Director::getInstance()->getVisibleSize();

  setContentSize(Size(_visibleSize.width, 100.0));
  setScale9Enabled(true);
  loadTextureNormal("listLine.png");
  setAnchorPoint({0, 1});
  // list1->setOpacity(255*0.5);
  _text = ui::Text::create();
  _text->setString(hanaData->title);
  _text->setPosition(Vec2(120, getContentSize().height / 2));
  _text->setFontSize(24);
  _text->setAnchorPoint({0, 0.5});
  _text->setTextHorizontalAlignment(cocos2d::TextHAlignment::LEFT);
  addChild(_text);

  _stamp = ui::ImageView::create("stamp2.png");
  _stamp->setAnchorPoint(Vec2(0.5, 0.5));
  _stamp->setPosition(Vec2(60, getContentSize().height / 2));
  _stamp->setOpacity(255 * 0.3);
  _stamp->setScale(0.8);
  addChild(_stamp);

  //    _inlineStamp = ui::ImageView::create("stamp2.png");
  //    _inlineStamp->setAnchorPoint(Vec2(0.5,0.5));
  //    _inlineStamp->setPosition(Vec2(60, getContentSize().height/2));
  //    _inlineStamp->setScale(0.5);
  //    _inlineStamp->setOpacity(255*1.0);
  //    _inlineStamp->setColor(ViewThemaModel::getMainColor(hanaData.thema));
  //    addChild(_inlineStamp);

  // setOpacityModifyRGB(true);
  setCascadeOpacityEnabled(true);
  setZoomScale(0);
  addTouchEventListener(CC_CALLBACK_2(ListCell::touchEffect, this));

  return true;
}

bool ListCell::init() {
  if (!ui::Button::init()) {
    return false;
  }
  _visibleSize = Director::getInstance()->getVisibleSize();

  setScale9Enabled(true);
  loadTextureNormal("listLine.png");
  setContentSize(Size(_visibleSize.width, 100));
  setAnchorPoint({0, 1});
  // list1->setOpacity(255*0.5);
  _text = ui::Text::create();
  _text->setString("新しいチェックカレンダーを追加");
  _text->setPosition(Vec2(120, this->getContentSize().height / 2));
  _text->setFontSize(24);
  _text->setAnchorPoint({0, 0.5});
  _text->setTextHorizontalAlignment(cocos2d::TextHAlignment::LEFT);
  addChild(_text);

  _stamp = ui::ImageView::create("plusIcon.png");
  _stamp->setAnchorPoint(Vec2(0.5, 0.5));
  _stamp->setPosition(Vec2(60, getContentSize().height / 2));
  _stamp->setOpacity(255 * 0.4);
  _stamp->setScale(0.8);
  addChild(_stamp);

  setCascadeOpacityEnabled(true);
  setZoomScale(0);
  ListCell::addTouchEventListener(CC_CALLBACK_2(ListCell::touchEffect, this));

  return true;
}

void ListCell::addCalenderEf() {
  auto textPos = _text->getPosition();
  auto scale1 = ScaleTo::create(0.05, 0.7);
  auto ease1 = EaseQuadraticActionIn::create(scale1);

  auto func1 = CallFunc::create([&]() {
    _stamp->loadTexture("stamp2.png");
    _text->setString("新しい目標");
    _text->setOpacity(0);
    _text->setPosition(Vec2(120 + 10, this->getContentSize().height / 2));

    auto move = MoveTo::create(0.1, Vec2(120, this->getContentSize().height / 2));
    auto fade = FadeIn::create(0.1);
    auto spawn = Spawn::create(move, fade, NULL);
    auto ease = EaseQuadraticActionOut::create(spawn);
    _text->runAction(ease);
  });

  auto scale2 = ScaleTo::create(0.1, 0.8);
  auto ease2 = EaseQuadraticActionOut::create(scale2);

  auto seq1 = Sequence::create(ease1, func1, ease2, NULL);
  _stamp->runAction(seq1);

  setEnabled(false);
}

void ListCell::setHighlighted(bool highlighted) {
  auto _selectEf = ui::ImageView::create("selectDayEf.png");
  _selectEf->setAnchorPoint(Vec2(0.5, 0.5));
  _selectEf->setPosition(_stamp->getPosition());
  _selectEf->setScale(0.8);
  //_selectEf->setColor(ViewThemaModel::getStampColor());
  _selectEf->setOpacity(255 * 0.3);
  addChild(_selectEf);

  _selectEf->runAction(RepeatRotate::create());

  _stamp->setOpacity(255 * 0.8);
  //    auto fade1 = FadeTo::create(0.4, 255*0.3);
  //    auto ease1 = EaseQuadraticActionIn::create(fade1);
  //    auto fade2 = FadeTo::create(0.4, 255*0.8);
  //    auto ease2 = EaseQuadraticActionOut::create(fade2);
  //    auto seq1 = Sequence::create(ease1,ease2,NULL);
  //    auto repeatForever = RepeatForever::create(seq1);
  //    _stamp->runAction(repeatForever);
}

void ListCell::touchEffect(Ref* pSender, ui::Widget::TouchEventType type) {
  switch (type) {
    case ui::Widget::TouchEventType::BEGAN:
      setOpacity(255 * 0.5);
      break;
    case ui::Widget::TouchEventType::MOVED:
      break;
    case ui::Widget::TouchEventType::CANCELED:
      setOpacity(255);
      break;
    case ui::Widget::TouchEventType::ENDED:
      setOpacity(255);
      break;
    default:
      break;
  }
}
