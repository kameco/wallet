#include "ListView.h"
#include "ViewThemaModel.h"

#include "Events.h"
#include "ActionUtil.h"

#include <time.h>

USING_NS_CC;

ListView* ListView::create() {
  ListView* layout = new (std::nothrow) ListView();
  if (layout && layout->init()) {
    layout->autorelease();
    return layout;
  }
  CC_SAFE_DELETE(layout);
  return nullptr;
}

bool ListView::init() {
  if (!ui::Layout::init()) {
    return false;
  }
  _appData = ChecalModel::sharedChecalModel();
  auto appData = _appData->getChecalModel();

  auto sizeDammy = -270;  // 270
  int cellCount = appData.size();
  if (cellCount != 5) {
    ++cellCount;
  }
  auto height = 100 * (cellCount) + 280;

  _visibleSize = Director::getInstance()->getVisibleSize();
  setAnchorPoint(Vec2(0, 1));
  setContentSize(Size(_visibleSize.width, height));

  setPosition(Vec2(0, _visibleSize.height + height));

  setBackGroundImage("headerBg.png");
  setBackGroundImageScale9Enabled(true);
  setColor(ViewThemaModel::getMainColor());

  auto posY = height - 110;
  for (int i = 0; i < appData.size(); ++i) {
    auto list1 = ListCell::create(appData[i]);
    list1->setPosition(Vec2(0, posY));
    addChild(list1);
    posY -= 100;
    list1->addTouchEventListener([=](Ref* pSender, cocos2d::ui::Widget::TouchEventType type) {
      list1->touchEffect(pSender, type);
      if (type == cocos2d::ui::Widget::TouchEventType::ENDED) {
        auto evt = EventCustom(Events::SELECTED_OPEN_CALENDER);
        auto data = appData[i];
        evt.setUserData(&data);
        getEventDispatcher()->dispatchEvent(&evt);
      }
    });

    if (appData[i]->id == _appData->getDispId()) {
      list1->setHighlighted(true);
    }

    auto deleteButton = ui::Button::create();
    deleteButton->loadTextureNormal("deleteButtonNormal.png");
    deleteButton->loadTexturePressed("deleteButtonPressed.png");
    deleteButton->setPosition(Vec2(580, posY + 50));
    deleteButton->setOpacity(255 * 0.5);
    addChild(deleteButton);
    deleteButton->addTouchEventListener([=](Ref* pSender, cocos2d::ui::Widget::TouchEventType type) {
      if (type == cocos2d::ui::Widget::TouchEventType::ENDED) {
        auto evt = EventCustom(Events::SELECTED_DELETE_CALENDER);
        auto data = appData[i];
        evt.setUserData(&data);
        getEventDispatcher()->dispatchEvent(&evt);
      }
    });
  }

  if (appData.size() < 5) {
    _pulsList = ListCell::create();
    _pulsList->setPosition(Vec2(0, posY));
    addChild(_pulsList);
    _pulsList->addTouchEventListener([=](Ref* pSender, cocos2d::ui::Widget::TouchEventType type) {
      _pulsList->touchEffect(pSender, type);
      if (type == cocos2d::ui::Widget::TouchEventType::ENDED) {
        _pulsList->addCalenderEf();
        auto delay = DelayTime::create(0.5);
        auto func = CallFunc::create([&]() {
          auto evt = EventCustom(Events::TOUCHED_CREATE_CALENDER_BUTTON);
          getEventDispatcher()->dispatchEvent(&evt);
        });
        auto seq = Sequence::create(delay, func, NULL);
        _pulsList->runAction(seq);
      }
    });

    posY -= 100;
  }

  posY -= 80;
  _closeButton = ui::Button::create();
  _closeButton->loadTextureNormal("closeButtonNormal.png");
  _closeButton->loadTexturePressed("closeButtonPressed.png");
  _closeButton->loadTextureDisabled("closeButtonDisabled.png");
  //_closeButton->setColor(ViewThemaModel::getBackgroundColor());
  //_closeButton->setFlippedX(true);
  _closeButton->setAnchorPoint(Vec2(0.5, 0.5));  // 115
  _closeButton->setPosition(Vec2(getContentSize().width / 2, posY));
  addChild(_closeButton);
  _closeButton->addTouchEventListener([this](Ref* pSender, cocos2d::ui::Widget::TouchEventType type) {
    if (type == cocos2d::ui::Widget::TouchEventType::ENDED) {
      CCLOG("_closeButton touch ended.");

      auto evt = EventCustom(Events::TOUCHED_CLOSE_MENU_BUTTON);
      getEventDispatcher()->dispatchEvent(&evt);
    }
  });

  if (appData.size() == 0) {
    _closeButton->setEnabled(false);
    _closeButton->setBright(false);
    _pulsList->setHighlighted(true);
  }

  _changeThemaListner = getEventDispatcher()->addCustomEventListener(Events::UPDATE_THEMA, [this](EventCustom* event) { setThema(); });

  open();

  return true;
}

void ListView::initList() {}

void ListView::setThema() {
  auto color = TintTo::create(0.2, ViewThemaModel::getMainColor());
  auto ease = EaseQuadraticActionOut::create(color);
  runAction(ease);
}

void ListView::open() {
  initList();
  auto act = SlideDownTo::create(Vec2(0, _visibleSize.height));
  runAction(act);
}

void ListView::close() {
  auto act = SlideUpTo::create(Vec2(0, _visibleSize.height + getContentSize().height));
  auto remove = RemoveSelf::create();
  auto seq = Sequence::create(act, remove, NULL);
  runAction(seq);
}

ListView::~ListView() { getEventDispatcher()->removeEventListener(_changeThemaListner); }
