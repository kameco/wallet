#include "SettingView.h"
#include "ViewThemaModel.h"

#include "Events.h"
#include "ActionUtil.h"

#include <time.h>

USING_NS_CC;

SettingView* SettingView::create() {
  SettingView* layout = new (std::nothrow) SettingView();
  if (layout && layout->init()) {
    layout->autorelease();
    return layout;
  }
  CC_SAFE_DELETE(layout);
  return nullptr;
}

bool SettingView::init() {
  if (!ui::Layout::init()) {
    return false;
  }

  _appData = ChecalModel::sharedChecalModel();  //うーんデータの取り扱いがたるい

  auto sizeDammy = -90;

  _visibleSize = Director::getInstance()->getVisibleSize();
  setAnchorPoint(Vec2(0, 1));
  setContentSize(Size(_visibleSize.width, 750 + sizeDammy));

  setPosition(Vec2(0, _visibleSize.height + 750 + sizeDammy));

  setBackGroundImage("headerBg.png");
  setBackGroundImageScale9Enabled(true);
  setColor(ViewThemaModel::getMainColor());

  auto posY = 640 + sizeDammy;
  auto todo = ui::Text::create();
  todo->setString("やること");
  todo->setFontSize(24);
  addChild(todo);
  todo->setAnchorPoint(Vec2(0, 1));
  todo->setPosition(Vec2(40, posY));
  // auto bg = ui::ImageView::create("dammyForm.png");
  _todoEditBox = ui::EditBox::create(Size(555, 60), "dammyForm.png");
  addChild(_todoEditBox);
  _todoEditBox->setFontName("HiraKaku-W3");
  _todoEditBox->setFontSize(26);
  _todoEditBox->setMaxLength(20);
  //_todoEditBox->setContentSize(Size(50, 300));
  _todoEditBox->setAnchorPoint(Vec2(0, 1));
  _todoEditBox->setPosition(Vec2(40, posY - 40));
  _todoEditBox->setPlaceHolder("例：運動する");
  _todoEditBox->setPlaceholderFontName("HiraKaku-W3");
  _todoEditBox->setPlaceholderFontSize(20);
  _todoEditBox->setPlaceholderFontColor(Color4B(0, 0, 0, 255 * 0.3));
  //_todoEditBox->setInputMode(ui::EditBox::InputMode::SINGLE_LINE);
  _todoEditBox->setOpacity(255 * 0.3);
  //_todoEditBox->setReturnType(ui::EditBox::KeyboardReturnType::DONE);
  _todoEditBox->setName("todo");
  _todoEditBox->setDelegate(this);

  posY -= 140;
  auto goalPoint = ui::Text::create();
  goalPoint->setString("目標日数");
  goalPoint->setFontSize(24);
  addChild(goalPoint);
  goalPoint->setAnchorPoint(Vec2(0, 1));
  goalPoint->setPosition(Vec2(40, posY));

  auto pointText = ui::TextBMFont::create("DAYS", "Font/test.fnt");
  pointText->setAnchorPoint(Vec2::ZERO);
  pointText->setColor(Color3B::WHITE);
  pointText->setScale(0.3);
  pointText->setAnchorPoint(Vec2(0, 1));
  pointText->setPosition(Vec2(40 + 270, posY - 80));
  pointText->setOpacity(255 * 0.5);
  addChild(pointText);

  _goalPointEditBox = ui::EditBox::create(Size(250, 60), ui::Scale9Sprite::create("dammyForm.png"));
  addChild(_goalPointEditBox);
  _goalPointEditBox->setFontName("HiraKaku-W3");
  _goalPointEditBox->setFontSize(26);
  _goalPointEditBox->setMaxLength(3);
  _goalPointEditBox->setAnchorPoint(Vec2(0, 1));
  _goalPointEditBox->setPosition(Vec2(40, posY - 40));
  _goalPointEditBox->setPlaceHolder("例：20");
  _goalPointEditBox->setPlaceholderFontName("HiraKaku-W3");
  _goalPointEditBox->setPlaceholderFontSize(26);
  _goalPointEditBox->setPlaceholderFontColor(Color4B(0, 0, 0, 255 * 0.3));
  _goalPointEditBox->setInputMode(ui::EditBox::InputMode::NUMERIC);
  _goalPointEditBox->setOpacity(255 * 0.3);
  _goalPointEditBox->setReturnType(ui::EditBox::KeyboardReturnType::DONE);
  _goalPointEditBox->setName("goalPoint");
  _goalPointEditBox->setDelegate(this);

  //    posY -= 140;
  //    auto pointType = ui::ImageView::create("textPointType.png");
  //    addChild(pointType);
  //    pointType->setAnchorPoint(Vec2(0, 1));
  //    pointType->setPosition(Vec2(40, posY));
  //
  //    auto formPointType0 = ui::Button::create();
  //    formPointType0->loadTextureNormal("pointType1.png");
  //    formPointType0->setAnchorPoint(Vec2(0, 1));//115
  //    formPointType0->setPosition(Vec2(40, posY-40));
  //    formPointType0->setZoomScale(0);
  //    addChild(formPointType0);
  //    auto formPointType1 = ui::Button::create();
  //    formPointType1->loadTextureNormal("pointType2.png");
  //    formPointType1->setAnchorPoint(Vec2(0, 1));//115
  //    formPointType1->setPosition(Vec2(_visibleSize.width-40-
  //    formPointType1->getContentSize().width, posY-40));
  //    formPointType1->setZoomScale(0);
  //    addChild(formPointType1);
  //    auto formPointSelected = ui::ImageView::create("pointTypeSelected.png");
  //    formPointSelected->setAnchorPoint(Vec2(0, 1));//115
  //    formPointSelected->setPosition(Vec2(40, posY-40));
  //    addChild(formPointSelected);
  //
  //    formPointType0->addTouchEventListener([=](Ref* pSender,
  //    cocos2d::ui::Widget::TouchEventType type){
  //        if (type == cocos2d::ui::Widget::TouchEventType::ENDED){
  //            _appData->setPointType(_id, 0);
  //            formPointSelected->stopAllActions();
  //            auto move = MoveTo::create(0.1, formPointType0->getPosition());
  //            auto ease = EaseQuadraticActionOut::create(move);
  //            formPointSelected->runAction(ease);
  //
  //            auto evt = EventCustom("updatePointType");
  //            int type = 0;
  //            evt.setUserData(&type);
  //            getEventDispatcher()->dispatchEvent(&evt);
  //        }
  //    });
  //
  //    formPointType1->addTouchEventListener([=](Ref* pSender,
  //    cocos2d::ui::Widget::TouchEventType type){
  //        if (type == cocos2d::ui::Widget::TouchEventType::ENDED){
  //            _appData->setPointType(_id, 1);
  //            formPointSelected->stopAllActions();
  //            auto move = MoveTo::create(0.1, formPointType1->getPosition());
  //            auto ease = EaseQuadraticActionOut::create(move);
  //            formPointSelected->runAction(ease);
  //
  //            auto evt = EventCustom("updatePointType");
  //            int type = 1;
  //            evt.setUserData(&type);
  //            getEventDispatcher()->dispatchEvent(&evt);
  //        }
  //    });

  posY -= 140;
  auto themaColor = ui::Text::create();
  themaColor->setString("テーマカラー");
  themaColor->setFontSize(24);
  addChild(themaColor);
  themaColor->setAnchorPoint(Vec2(0, 1));
  themaColor->setPosition(Vec2(40, posY));

  for (int i = 0; i < 4; ++i) {
    auto colorTip = ui::Button::create();
    colorTip->loadTextureNormal("colorTip.png");
    colorTip->setAnchorPoint(Vec2(0, 1));  // 115
    colorTip->setPosition(Vec2(40 + 70 * i, posY - 40));
    colorTip->setColor(ViewThemaModel::getMainColor(static_cast<ViewThemaModel::Type>(i)));
    addChild(colorTip);

    auto frame = ui::ImageView::create("colorSelected.png");
    frame->setAnchorPoint(Vec2(0, 1));  // 115
    frame->setPosition(Vec2(40 + 70 * i, posY - 40));
    frame->setOpacity(255 * 0.3);
    addChild(frame);

    colorTip->addTouchEventListener([=](Ref* pSender, cocos2d::ui::Widget::TouchEventType type) {
      if (type == cocos2d::ui::Widget::TouchEventType::ENDED) {
        _appData->setThema(static_cast<ViewThemaModel::Type>(i));
        ViewThemaModel::setThema(static_cast<ViewThemaModel::Type>(i));
        if (_colorSelected) {
          auto move = MoveTo::create(0.1, colorTip->getPosition());
          auto ease = EaseQuadraticActionInOut::create(move);
          _colorSelected->runAction(ease);
        }
      }
    });

    _colorTips.push_back(colorTip);
  }

  _colorSelected = ui::ImageView::create("colorSelected.png");
  _colorSelected->setAnchorPoint(Vec2(0, 1));  // 115
  _colorSelected->setPosition(_colorTips[0]->getPosition());
  addChild(_colorSelected);

  posY -= 180;
  _closeButton = ui::Button::create();
  _closeButton->loadTextureNormal("closeButtonNormal.png");
  _closeButton->loadTexturePressed("closeButtonPressed.png");
  //_closeButton->setColor(ViewThemaModel::getBackgroundColor());
  //_closeButton->setFlippedX(true);
  _closeButton->setAnchorPoint(Vec2(0.5, 0.5));  // 115
  _closeButton->setPosition(Vec2(getContentSize().width / 2, posY));
  addChild(_closeButton);

  _closeButton->addTouchEventListener([this](Ref* pSender, cocos2d::ui::Widget::TouchEventType type) {
    if (type == cocos2d::ui::Widget::TouchEventType::ENDED) {
      CCLOG("_closeButton touch ended.");
      auto evt = EventCustom(Events::TOUCHED_CLOSE_SETTING_BUTTON);
      getEventDispatcher()->dispatchEvent(&evt);
    }
  });

  getEventDispatcher()->addCustomEventListener(Events::UPDATE_THEMA, [this](EventCustom* event) { setThema(); });

  return true;
}

void SettingView::setThema() {
  auto color = TintTo::create(0.2, ViewThemaModel::getMainColor());
  auto ease = EaseQuadraticActionOut::create(color);
  runAction(ease);
  // setColor(ViewThemaModel::getMainColor());
}

void SettingView::open() {
  updateData();
  auto act = SlideDownTo::create(Vec2(0, _visibleSize.height));
  runAction(act);
}

void SettingView::close() {
  auto act = SlideUpTo::create(Vec2(0, _visibleSize.height + getContentSize().height));
  runAction(act);
}

void SettingView::updateData() {
  _goalPointEditBox->setText(cocos2d::StringUtils::toString(_appData->getGoalPoint()).c_str());
  _todoEditBox->setText(_appData->getTitle().c_str());
  auto selectedColorTip = _colorTips[static_cast<int>(_appData->getThema())];
  _colorSelected->setPosition(selectedColorTip->getPosition());
}

void SettingView::editBoxEditingDidBegin(ui::EditBox* editBox) {}

void SettingView::editBoxEditingDidEnd(ui::EditBox* editBox) {}

void SettingView::editBoxTextChanged(ui::EditBox* editBox, const std::string& text) {}

void SettingView::editBoxReturn(ui::EditBox* editBox) {
  if (editBox->getName() == "todo") {
    _appData->setTitle(editBox->getText());
  } else if (editBox->getName() == "goalPoint") {
    auto val = editBox->getText();
    _appData->setGoalPoint(atoi(val));
  }
}
