#pragma once

#include "ChecalModel.h"
#include "Stamp.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"

#include "DayCellView.h"
#include "WeekLineView.h"

class CalendarView : public cocos2d::ui::Layout {
public:
  virtual bool init(tm dispTime);
  static CalendarView *create(tm dispTime);

  void targetingDayCell(int day);
  void setTotalPoint(int totalPoint, int goalPoint, bool runAnimation = false);

private:
  cocos2d::Size _visibleSize;
  bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event);
  void onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *event);
  void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event);

  cocos2d::Vector<DayCellView *> _dayCells;
  DayCellView *_selectDayCell;
  DayCellView *_todayCell;

  cocos2d::Vec2 _touchBeganPos;
  tm _dispTime;

  cocos2d::ui::Button *_backButton = nullptr;
  cocos2d::ui::Button *_nextButton = nullptr;

  void addEvent();
  void createCalendarView();

  void targetingDayCell(cocos2d::Touch *touch);
  void targetingDayCell(DayCellView *dayCell);
  void targetingDayCell(bool target);

  ~CalendarView();
  std::vector<cocos2d::EventListenerCustom *> _listners;
};
