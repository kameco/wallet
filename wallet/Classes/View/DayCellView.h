#pragma once

#include "cocos2d.h"
#include "ui/CocosGUI.h"

class DayCellView : public cocos2d::ui::Widget {
public:
  enum class CellState { NONE = -1, INVISIBLE = 1, ACTIVE, INACTIVE };
  static DayCellView *create(tm dispTime);
  CREATE_FUNC(DayCellView);

  CC_SYNTHESIZE(tm, _dispTime, DispTime);
  CC_SYNTHESIZE(bool, _isToday, IsToday);
  CC_SYNTHESIZE(CellState, _cellState, CellState);

  void runDisableAction();
  void isSelect(bool select);

  ~DayCellView();

private:
  bool init(tm dispTime);
  bool init();
  cocos2d::EventListenerCustom *_changeThemaListner = nullptr;

  cocos2d::Size _visibleSize;
  cocos2d::ui::ImageView *_selectEf = nullptr;
  cocos2d::ui::TextBMFont *_dayLabel = nullptr;

  void setThema();
};
