#pragma once

#include "cocos2d.h"
#include "ui/CocosGUI.h"

#include "DayCellView.h"

class WeekLineView : public cocos2d::ui::Layout {
public:
  static WeekLineView *create(tm dispTime, int weekCount);
  ~WeekLineView();

private:
  bool init(tm dispTime, int weekCount);
  void addEvents();
  void createView();

  tm _dispTime;
  int _weekCount = 0;
};
