#include "ViewThemaModel.h"
#include "WeekLineView.h"

#include "ChecalModel.h"

#include "ActionUtil.h"
#include "DateUtil.h"
#include "Events.h"

#include <time.h>

USING_NS_CC;

WeekLineView *WeekLineView::create(tm dispTime, int weekCount) {
  WeekLineView *layout = new (std::nothrow) WeekLineView();
  if (layout && layout->init(dispTime, weekCount)) {
    layout->autorelease();
    return layout;
  }
  CC_SAFE_DELETE(layout);
  return nullptr;
}

bool WeekLineView::init(tm dispTime, int weekCount) {
  if (!ui::Layout::init()) {
    return false;
  }

  _dispTime = dispTime;
  _weekCount = weekCount;

  CCLOG("create:%i", weekCount);

  createView();
  addEvents();

  //

  return true;
}

void WeekLineView::createView() {

  auto _visibleSize = Director::getInstance()->getVisibleSize();
  auto size = (_visibleSize.width - 80 + 20) / 7;
  setPositionY(520 - size * _weekCount);

  int firstWeek = DateUtil::getFirstDayWeekday(_dispTime);
  auto lastDay = DateUtil::getLastDayTime(_dispTime);

  cocos2d::Vector<DayCellView *> dayCells;
  int count = _weekCount * DateUtil::WEEK.size();
  int day = 1;
  day += _weekCount * DateUtil::WEEK.size() - firstWeek;

  struct tm dayTime = _dispTime;
  dayTime.tm_mday = day;

  for (int j = 0; j < (int)DateUtil::WEEK.size(); j++) {
    if (firstWeek <= count && lastDay.tm_mday >= day) {
      auto cell = DayCellView::create(dayTime);
      cell->setCascadeOpacityEnabled(true);
      cell->setAnchorPoint(Vec2::ZERO);
      cell->setPositionX(cell->getContentSize().width * j + -10);
      addChild(cell);
      dayCells.pushBack(cell);

      DateUtil::getWeekCountOfMonth(dayTime);
    }
    dayTime = DateUtil::getNextDay(dayTime);
    ++day;
    ++count;
  }
}

void WeekLineView::addEvents() {
  getEventDispatcher()->addCustomEventListener(
      Events::SELECTED_DAY_CELL, [this](EventCustom *event) {

        auto data = *static_cast<tm *>(event->getUserData());
        auto selectedWeekCount = DateUtil::getWeekCountOfMonth(data);

        this->setVisible(selectedWeekCount == _weekCount);

        CCLOG("selectedWeekCount:%i", selectedWeekCount);
        CCLOG("_weekCount:%i", _weekCount);

      });

  getEventDispatcher()->addCustomEventListener(Events::TOUCHED_CALENDER_BUTTON,
                                               [this](EventCustom *event) {
                                                 //
                                                 this->setVisible(true);
                                               });
}

WeekLineView::~WeekLineView() {
  // getEventDispatcher()->removeEventListener(_changeThemaListner);
}
