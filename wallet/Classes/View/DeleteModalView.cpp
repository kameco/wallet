#include "DeleteModalView.h"
#include "ViewThemaModel.h"

#include "Events.h"

USING_NS_CC;

DeleteModalView* DeleteModalView::create(const std::shared_ptr<ChecalModel::CalendarData> hanaData) {
  DeleteModalView* layout = new (std::nothrow) DeleteModalView();
  if (layout && layout->init(hanaData)) {
    layout->autorelease();
    return layout;
  }
  CC_SAFE_DELETE(layout);
  return nullptr;
}

bool DeleteModalView::init(const std::shared_ptr<ChecalModel::CalendarData> hanaData) {
  if (!ui::Layout::init()) {
    return false;
  }

  // setVisible(false);
  _visibleSize = Director::getInstance()->getVisibleSize();

  setPosition(Vec2(_visibleSize.width / 2, _visibleSize.height / 2 + 50));
  setCascadeOpacityEnabled(true);
  setBackGroundImage("modalFlame.png");
  setBackGroundImageScale9Enabled(true);
  setColor(Color3B(120, 110, 110));
  setOpacity(255 * 0.90);

  setContentSize(Size(500, 350));
  setAnchorPoint({0.5, 0.5});

  auto title = ui::Text::create();
  title->setString(hanaData->title);
  title->setPosition(Vec2(getContentSize().width / 2, getContentSize().height - 100));
  title->setFontSize(24);
  addChild(title);

  auto text = ui::Text::create();
  text->setString("消去してもよろしいですか？");
  text->setPosition(Vec2(getContentSize().width / 2, getContentSize().height - 150));
  text->setFontSize(24);
  addChild(text);

  auto cancelButton = ui::Button::create();
  cancelButton->loadTextureNormal("pointFlame.png");
  // cancelButton->loadTexturePressed("closeButtonPressed.png");
  //_closeButton->setColor(ViewThemaModel::getBackgroundColor());
  //_closeButton->setFlippedX(true);
  cancelButton->setAnchorPoint(Vec2(0.5, 0.5));  // 115
  cancelButton->setPosition(Vec2(getContentSize().width / 3 - 20, getContentSize().height - 265));
  cancelButton->setZoomScale(0);
  addChild(cancelButton);
  auto cancelLabel = ui::TextBMFont::create("Cancel", "Font/test.fnt");
  cancelLabel->setAnchorPoint({0.5, 0.6});
  cancelLabel->setColor(ViewThemaModel::getBackgroundColor());
  cancelLabel->setScale(0.4);
  cancelLabel->setPosition(cancelButton->getContentSize() / 2);
  cancelButton->addChild(cancelLabel);
  cancelButton->setCascadeOpacityEnabled(true);
  cancelButton->addTouchEventListener([=](Ref* pSender, cocos2d::ui::Widget::TouchEventType type) {
    if (type == cocos2d::ui::Widget::TouchEventType::BEGAN) {
      cancelButton->setOpacity(255 * 0.5);
    } else if (type == cocos2d::ui::Widget::TouchEventType::ENDED) {
      cancelButton->setOpacity(255);
      close();
    } else if (type == cocos2d::ui::Widget::TouchEventType::CANCELED) {
      cancelButton->setOpacity(255);
    }
  });

  auto deleteButton = ui::Button::create();
  deleteButton->loadTextureNormal("pointFlame.png");
  // deleteButton->loadTexturePressed("closeButtonPressed.png");
  //_closeButton->setColor(ViewThemaModel::getBackgroundColor());
  //_closeButton->setFlippedX(true);
  deleteButton->setAnchorPoint(Vec2(0.5, 0.5));  // 115
  deleteButton->setPosition(Vec2(getContentSize().width / 3 * 2 + 20, getContentSize().height - 265));
  deleteButton->setZoomScale(0);
  addChild(deleteButton);
  auto deleteLabel = ui::TextBMFont::create("Delete", "Font/test.fnt");
  deleteLabel->setAnchorPoint({0.5, 0.6});
  deleteLabel->setColor(ViewThemaModel::getBackgroundColor());
  deleteLabel->setScale(0.4);
  deleteLabel->setPosition(deleteButton->getContentSize() / 2);
  deleteButton->addChild(deleteLabel);
  deleteButton->setCascadeOpacityEnabled(true);
  deleteButton->addTouchEventListener([=](Ref* pSender, cocos2d::ui::Widget::TouchEventType type) {
    if (type == cocos2d::ui::Widget::TouchEventType::BEGAN) {
      deleteButton->setOpacity(255 * 0.5);
    } else if (type == cocos2d::ui::Widget::TouchEventType::ENDED) {
      deleteButton->setOpacity(255);
      close();
      auto evt = EventCustom(Events::TOUCHED_DELETE_FIX_BUTTON);
      auto data = hanaData;
      evt.setUserData(&data);
      getEventDispatcher()->dispatchEvent(&evt);

    } else if (type == cocos2d::ui::Widget::TouchEventType::CANCELED) {
      deleteButton->setOpacity(255);
    }
  });

  setScale(0);
  open();

  // モーダル処理
  _listener = EventListenerTouchOneByOne::create();
  _listener->setSwallowTouches(true);
  _listener->onTouchBegan = [](Touch* touch, Event* event) -> bool { return true; };
  auto dispatcher = Director::getInstance()->getEventDispatcher();
  dispatcher->addEventListenerWithSceneGraphPriority(_listener, this);

  return true;
}

void DeleteModalView::open() {
  auto scale1 = ScaleTo::create(0.12, 1.0);
  auto rotate1 = RotateTo::create(0.12, 0);
  auto spawn1 = Spawn::create(scale1, rotate1, NULL);
  auto ease1 = EaseQuadraticActionIn::create(spawn1);

  auto scale2 = ScaleTo::create(0.03, 0.9);
  auto ease2 = EaseQuadraticActionOut::create(scale2);
  auto seq1 = Sequence::create(ease1, ease2, NULL);
  runAction(seq1);
}

void DeleteModalView::close() {
  auto scale1 = ScaleTo::create(0.1, 0);
  auto ease1 = EaseQuadraticActionIn::create(scale1);
  auto remove = RemoveSelf::create();
  auto seq1 = Sequence::create(ease1, remove, NULL);
  runAction(seq1);
}

DeleteModalView::~DeleteModalView() {
  // CCLOG("nununununununu?");
  // getEventDispatcher()->removeEventListenersForTarget(this);
}
