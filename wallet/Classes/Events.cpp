//
//  ChecalEvents.cpp
//  HanaCal
//
//  Created by HidakaKaname on 2015/07/19.
//
//

#include "Events.h"

std::string Events::TOUCHED_CALENDER_BUTTON = "touched_calender_button";
std::string Events::SELECTED_DAY_CELL = "selected_day_cell";

// メイン画面
std::string Events::SELECTED_MOVE_CALENDER_PAGE = "selected_move_calender_page";
std::string Events::TOUCHED_TODAY_CHECK_BUTTON = "touched_today_check_button";
std::string Events::SELECTED_CALENDER_CELL = "selected_calender_cell";
std::string Events::SELECTED_STAMP = "selected_stamp";

// 設定画面
std::string Events::TOUCHED_CLOSE_SETTING_BUTTON =
    "touched_close_setting_button";

// メニュー画面
std::string Events::TOUCHED_CLOSE_MENU_BUTTON = "touched_close_menu_button";
std::string Events::TOUCHED_CREATE_CALENDER_BUTTON =
    "touched_create_calender_button";
std::string Events::SELECTED_OPEN_CALENDER = "selected_open_caldnder";
std::string Events::SELECTED_DELETE_CALENDER = "selected_delete_calender";

// デリートモーダル
std::string Events::TOUCHED_DELETE_CANCEL_BUTTON =
    "touched_delete_cancel_button";
std::string Events::TOUCHED_DELETE_FIX_BUTTON = "touched_delete_fix_button";

// グラフ
std::string Events::TOUCHED_GRAPH_BUTTON = "touched_graph_button";
std::string Events::TOUCHED_CLOSE_GRAPH_BUTTON = "touched_close_graph_button";

// データ更新
std::string Events::UPDATE_TITLE = "update_title";
std::string Events::UPDATE_POINT = "update_point";
std::string Events::UPDATE_THEMA = "update_thema";
std::string Events::MISSION_COMPLETED = "mission_completed";
std::string Events::CLOSED_MISSION_COMPLETED_EFFECT =
    "closed_mission_completed_effect";