#pragma once

#include "cocos2d.h"
#include "ui/CocosGUI.h"

#include "CalendarView.h"
#include "FooterView.h"

class WalletScene : public cocos2d::ui::Layout {
public:
  static cocos2d::Scene *createScene();
  CREATE_FUNC(WalletScene);

private:
  virtual bool init();
  FooterView *_foorterView = nullptr;
  CalendarView *_calendarView = nullptr;
};
