#include "PreferenceManager.h"

// json
// http://cocos2d-x.org/docs/manual/framework/native/v3/json-parse/zh#
#include "json/document.h"
#include "json/rapidjson.h"
#include "json/stringbuffer.h"
#include "json/writer.h"

void PreferenceManager::save(const ChecalModel::AllData allData) {
  rapidjson::Document document;
  document.SetObject();
  rapidjson::Document::AllocatorType &allocator = document.GetAllocator();

  // rapidjson::Value appData(rapidjson::kObjectType);
  rapidjson::Value appData(rapidjson::kArrayType);

  std::map<int, std::map<int, std::string>> timesV;
  std::map<int, std::map<int, std::map<int, std::string>>> days;

  for (int k = 0; k < allData.calenderDataList.size(); ++k) {

    rapidjson::Value hanaVal(rapidjson::kObjectType);
    hanaVal.AddMember("id", allData.calenderDataList[k]->id, allocator);
    // ??hanaVal.AddMember("title", allData.calenderDataList[k]->title.c_str(),
    // allocator);
    hanaVal.AddMember("pointType", allData.calenderDataList[k]->pointType,
                      allocator);
    hanaVal.AddMember("goalPoint", allData.calenderDataList[k]->goalPoint,
                      allocator);
    hanaVal.AddMember("thema",
                      static_cast<int>(allData.calenderDataList[k]->thema),
                      allocator);

    rapidjson::Value allVal(rapidjson::kObjectType);
    rapidjson::Value times(rapidjson::kArrayType);
    auto j = 0;

    auto monthData = allData.calenderDataList[k]->monthDataList;

    // http://homepage2.nifty.com/well/map.html
    for (std::map<int, std::map<int, Stamp::Type>>::iterator itpairstri =
             monthData.begin();
         itpairstri != monthData.end(); itpairstri++) {
      // イテレータは pair<const string, int> 型なので、
      int key = itpairstri->first; // イテレータからキーが得られる。
      std::map<int, Stamp::Type> val =
          itpairstri->second; // イテレータから値が得られる。

      rapidjson::Value monthVal(rapidjson::kObjectType);
      timesV[k][j] = "time" + cocos2d::StringUtils::toString(key);
      CCLOG("time %s", timesV[k][j].c_str());

      for (int i = 1; i <= 31; ++i) {
        days[k][key][i] = "day" + cocos2d::StringUtils::toString(i);
        auto stamp = static_cast<int>(
            allData.calenderDataList[k]->monthDataList[key][i]);
        //??monthVal.AddMember(days[k][key][i].c_str(), stamp, allocator);
      }

      //??allVal.AddMember(timesV[k][j].c_str(), monthVal, allocator);
      times.PushBack(key, allocator);
      ++j;
    }
    hanaVal.AddMember("monthData", allVal, allocator);
    hanaVal.AddMember("times", times, allocator);
    appData.PushBack(hanaVal, allocator);
  }

  document.AddMember("appData", appData, allocator);
  document.AddMember("lastDispId", allData.dispId, allocator);

  rapidjson::StringBuffer buffer;
  rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
  document.Accept(writer);

  // セーブ
  cocos2d::UserDefault *_userDef = cocos2d::UserDefault::getInstance();
  _userDef->setStringForKey("appData", buffer.GetString());

  CCLOG("save = %s", buffer.GetString());
}

ChecalModel::AllData PreferenceManager::load() {
  cocos2d::UserDefault *_userDef = cocos2d::UserDefault::getInstance();
  auto jsonStr = _userDef->getStringForKey("appData");
  CCLOG("load %s", jsonStr.c_str());

  // 一旦データ初期化
  // _allData.calenderDataList.clear();

  rapidjson::Document d;
  d.Parse<0>(jsonStr.c_str());
  if (d.HasParseError()) {
    CCLOG("GetParseError %s\n", d.GetParseError());
  }

  if (!d.IsObject() || !d.HasMember("appData")) {
    CCLOG("データがない");
    jsonStr = "{\"appData\":[],"
              "\"lastDispId\":-1"
              "}";
    d.Parse<0>(jsonStr.c_str());
  }

  // http://qiita.com/k2ymg/items/eef3b15eaa27a89353ab
  rapidjson::Value &val = d["appData"];
  auto appSize = val.Size(); // SizeType

  ChecalModel::AllData allData;
  allData.dispId = d["lastDispId"].GetInt();

  for (int k = 0; k < appSize; ++k) {
    rapidjson::Value &hanaVal = val[k];

    ChecalModel::CalendarData hanaData;
    hanaData.id = hanaVal["id"].GetInt();
    hanaData.title = hanaVal["title"].GetString();
    hanaData.pointType = hanaVal["pointType"].GetInt();
    hanaData.goalPoint = hanaVal["goalPoint"].GetInt();
    hanaData.thema =
        static_cast<ViewThemaModel::Type>(hanaVal["thema"].GetInt());

    rapidjson::Value &allVal = hanaVal["monthData"];

    rapidjson::Value &timesVal = hanaVal["times"];
    std::vector<int> times;
    int timeSize = timesVal.Size();
    for (int i = 0; i < timeSize; ++i) {
      auto t = timesVal[rapidjson::SizeType(i)].GetInt();
      times.push_back(t);

      // int dammyTime = 201506;
      std::map<int, std::string> timesV;

      timesV[i] = "time" + cocos2d::StringUtils::toString(t);
      rapidjson::Value &monthVal = allVal[timesV[i].c_str()];

      std::map<int, Stamp::Type> monthData;
      for (int j = 1; j < 32; ++j) {
        auto m = "day" + cocos2d::StringUtils::toString(j);
        if (monthVal.IsObject() && monthVal.HasMember(m.c_str())) {
          monthData[j] = static_cast<Stamp::Type>(monthVal[m.c_str()].GetInt());
        }
      }
      hanaData.monthDataList[t] = monthData;
    }
    auto data = std::make_shared<ChecalModel::CalendarData>(hanaData);
    allData.calenderDataList.push_back(data);
  }
  return allData;
}