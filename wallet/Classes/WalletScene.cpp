#include "WalletScene.h"
#include "ui/CocosGUI.h"

#include "DateUtil.h"
#include "Wallet.h"

#include "CalendarView.h"

USING_NS_CC;

Scene *WalletScene::createScene() {
  auto scene = Scene::create();
  auto layer = WalletScene::create();
  scene->addChild(layer);
  return scene;
}

bool WalletScene::init() {
  if (!Layout::init()) {
    return false;
  }

  CCLOG("ログを出すお！！！！");

  auto visibleSize = Director::getInstance()->getVisibleSize();
  this->setContentSize(visibleSize);
  this->setBackGroundColorType(BackGroundColorType::SOLID);
  this->setBackGroundColor(Color3B(255, 255, 255));

  _foorterView = FooterView::create();
  addChild(_foorterView);

  _calendarView = CalendarView::create(DateUtil::getCurrentTime());
  addChild(_calendarView);

  return true;
}